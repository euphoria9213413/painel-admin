export const env = {
  main_uri: process.env.REACT_APP_MAIN_URI,
  api_uri: process.env.REACT_APP_API_URI,
  react_app_ecommerce_uri: process.env.REACT_APP_ECOMMERCE_URI,
  per_page: Number(process.env.REACT_APP_PER_PAGE),
};
