import { api, headers } from "@/config/axiosInstance";
import { env } from "@/config/env";

import {
  SaleExportQueryDto,
  SaleIndexPaginatedDto,
  SaleIndexQueryDto,
} from "@/pages/sales/types";

export const useSale = () => {
  const getSales = (
    storeId: number,
    { page, graduateName, buyerName, accountName }: SaleIndexQueryDto
  ) => {
    let params = `page=${page}&perPage=${env.per_page}`;

    if (graduateName) {
      params = `${params}&graduateName=${graduateName}`;
    }

    if (buyerName) {
      params = `${params}&buyerName=${buyerName}`;
    }

    if (accountName) {
      params = `${params}&accountName=${accountName}`;
    }

    return api.get<SaleIndexPaginatedDto>(`/sale/${storeId}/index?${params}`);
  };

  const exportData = (
    storeId: number,
    { page, graduateName, buyerName, accountName }: SaleExportQueryDto
  ) => {
    let params = `page=${page}&perPage=${env.per_page}`;

    if (graduateName) {
      params = `${params}&graduateName=${graduateName}`;
    }

    if (buyerName) {
      params = `${params}&buyerName=${buyerName}`;
    }

    if (accountName) {
      params = `${params}&accountName=${accountName}`;
    }

    return api.get(`/sale/${storeId}/export?${params}`, {
      headers: { ...headers, Accept: "text/xlsx" },
      responseType: "blob",
    });
  };

  const statusUpdate = (id: number, status: string): Promise<void> => {
    return api.patch(`/sale/${id}/status`, { status });
  };

  const edit = (id: number, data: any): Promise<void> => {
    return api.patch(`/sale/${id}/edit`, data);
  };

  return {
    getSales,
    exportData,
    statusUpdate,
    edit,
  };
};
