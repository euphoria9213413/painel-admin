import { api, headers } from "@/config/axiosInstance";
import { env } from "@/config/env";
import {
  GraduateByCodeDto,
  GraduateIndexPaginatedDto,
  GraduateIndexQueryDto,
  GraduateStoreDto,
} from "@/pages/graduates/types";

export const useGraduate = () => {
  const getGraduates = ({
    page,
    storeId,
    codeOrNameOrClass,
    situation,
  }: GraduateIndexQueryDto) => {
    let params = `page=${page}&perPage=${env.per_page}`;

    if (codeOrNameOrClass) {
      params = `${params}&codeOrNameOrClass=${codeOrNameOrClass}`;
    }

    if (situation) {
      params = `${params}&situation=${situation}`;
    }

    return api.get<GraduateIndexPaginatedDto>(
      `/graduate/${storeId}/index?${params}`
    );
  };

  const graduateStore = (storeId: number, data: GraduateStoreDto) => {
    return api.post<GraduateIndexPaginatedDto>(
      `/graduate/${storeId}/create`,
      data
    );
  };

  const graduateSituationUpdate = (id: number, situation: boolean) => {
    return api.patch(`/graduate/${id}/situation-update`, { situation });
  };

  const graduateImport = (storeId: number, file: any) => {
    const formData = new FormData();
    formData.append("file", file);

    return api.post(`graduate/${storeId}/file`, formData, {
      headers: { ...headers, "Content-Type": "multipart/form-data" },
    });
  };

  const getGraduateByCode = (storeId: number, code: string) => {
    return api.get<GraduateByCodeDto>(`/graduate/${storeId}/${code}`);
  };

  return {
    getGraduates,
    graduateStore,
    graduateSituationUpdate,
    graduateImport,
    getGraduateByCode,
  };
};
