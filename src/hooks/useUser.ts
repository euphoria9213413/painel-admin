import { api } from "@/config/axiosInstance";
import { env } from "@/config/env";
import { LoginAuthenticated } from "@/pages/login/types/login";
import {
  ProfileDataDto,
  UserDataDto,
  UserEmailUpdateDto,
  UserIndexPaginatedDto,
  UserIndexQueryDto,
  UserPasswordUpdateDto,
  UserStoreDto,
} from "@/pages/users/types";

export const useUser = () => {
  const getUsers = ({
    nameOrEmail,
    profileType,
    situation,
    page,
  }: UserIndexQueryDto) => {
    let params = `page=${page}&perPage=${env.per_page}`;

    if (nameOrEmail) {
      params = `${params}&nameOrEmail=${nameOrEmail}`;
    }

    if (profileType) {
      params = `${params}&profileType=${profileType}`;
    }

    if (situation) {
      params = `${params}&situation=${situation}`;
    }

    return api.get<UserIndexPaginatedDto>(`/account/index?${params}`);
  };

  const userStore = (data: UserStoreDto) => {
    return api.post<void>(`/account/create`, data);
  };

  const userSituationUpdate = (id: number, situation: boolean) => {
    return api.patch<void>(`/account/${id}/situation-update`, { situation });
  };

  const userPasswordUpdate = (id: number, data: UserPasswordUpdateDto) => {
    return api.patch<LoginAuthenticated>(
      `/account/${id}/password-update`,
      data
    );
  };

  const userEmailUpdate = (id: number, data: UserEmailUpdateDto) => {
    return api.patch(`/account/${id}/email-update`, data);
  };

  const getProfiles = () => {
    return api.get<ProfileDataDto[]>("profile/index");
  };

  const getUser = (id: number) => {
    return api.get<UserDataDto>(`account/${id}`);
  };

  const userUpdate = (id: number, data: UserStoreDto) => {
    return api.put(`account/${id}`, data);
  };

  return {
    getUsers,
    userStore,
    userSituationUpdate,
    userPasswordUpdate,
    userEmailUpdate,
    getProfiles,
    getUser,
    userUpdate,
  };
};
