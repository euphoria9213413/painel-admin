import { api } from "@/config/axiosInstance";
import {
  LoginAuthenticated,
  LoginSubmitDto,
  StoreAccountDto,
} from "@/pages/login/types/login";
import { UpdateAccountPasswordDto } from "@/pages/newPassword/types/newPassword";
import { RecoverPasswordDto } from "@/pages/recoverPassword/types";
import { LocalStorageEnum } from "@/utils/localStorage";

export const useLogin = () => {
  const isAuthenticated = !!localStorage.getItem(LocalStorageEnum.ACCESS_TOKEN);

  const doLogin = (data: LoginSubmitDto) => {
    return api.post<LoginAuthenticated>("/account/login", data);
  };

  const setAuthentication = (authentication: LoginAuthenticated) =>
    localStorage.setItem(
      LocalStorageEnum.ACCESS_TOKEN,
      JSON.stringify(authentication)
    );

  const removeAuthentication = () =>
    localStorage.removeItem(LocalStorageEnum.ACCESS_TOKEN);

  const getAccount = (): LoginAuthenticated | undefined => {
    const authentication = localStorage.getItem(LocalStorageEnum.ACCESS_TOKEN);

    if (!authentication) return;

    return JSON.parse(authentication);
  };

  const doStore = (data: StoreAccountDto) => {
    return api.post<void>("/account/create", data);
  };

  const doSendPasswordRecoverEmail = (data: RecoverPasswordDto) => {
    return api.post("/password-recover", data);
  };

  const doNewPassword = (data: UpdateAccountPasswordDto, token: string) => {
    return api.post(`/password-recover/${token}`, data);
  };

  return {
    isAuthenticated,
    doLogin,
    setAuthentication,
    removeAuthentication,
    getAccount,
    doStore,
    doSendPasswordRecoverEmail,
    doNewPassword,
  };
};
