import { api, headers } from "@/config/axiosInstance";
import { env } from "@/config/env";
import {
  ProductDataDto,
  ProductStoreDto,
  StoreCreateDto,
  StoreDataDto,
  StoreIndexPaginatedDto,
  StoreIndexQueryDto,
  StoreNameDto,
} from "@/pages/stores/types";

export const useStore = () => {
  const getStores = ({ page, name, situation }: StoreIndexQueryDto) => {
    let params = `page=${page}&perPage=${env.per_page}`;

    if (name) {
      params = `${params}&name=${name}`;
    }

    if (situation) {
      params = `${params}&situation=${situation}`;
    }

    return api.get<StoreIndexPaginatedDto>(`/store/index?${params}`);
  };

  const storeSituationUpdate = (id: number, situation: boolean) => {
    return api.patch(`/store/${id}/situation-update`, { situation });
  };

  const getStoreName = (id: number) => {
    return api.get<StoreNameDto>(`/store/${id}/name`);
  };

  const storeCreate = (data: StoreCreateDto) => {
    return api.post<StoreDataDto>("store/create", data);
  };

  const storeUpdate = (id: number, data: StoreCreateDto) => {
    return api.put<StoreDataDto>(`store/${id}`, data);
  };

  const storeMediasUpload = (
    storeId: number,
    desktopFile?: File,
    mobileFile?: File
  ) => {
    const formData = new FormData();
    desktopFile && formData.append("desktopMedia", desktopFile);
    mobileFile && formData.append("mobileMedia", mobileFile);

    return api.post(`store/${storeId}/medias`, formData, {
      headers: { ...headers, "Content-Type": "multipart/form-data" },
    });
  };

  const getStore = (id: number) => {
    return api.get<StoreDataDto>(`/store/${id}`);
  };

  const productCreate = (storeId: number, data: ProductStoreDto) => {
    return api.post<ProductDataDto>(`/product/${storeId}/create`, data);
  };

  const productUpdate = (id: number, data: ProductStoreDto) => {
    return api.put<ProductDataDto>(`/product/${id}`, data);
  };

  const productDelete = (id: number) => {
    return api.delete<void>(`/product/${id}`);
  };

  return {
    getStores,
    storeSituationUpdate,
    getStoreName,
    storeCreate,
    storeUpdate,
    storeMediasUpload,
    getStore,
    productCreate,
    productUpdate,
    productDelete,
  };
};
