import { Route, Routes } from "react-router";
import { Login } from "./pages/login";
import { NewAccount } from "./pages/newAccount";
import { ForgotPassword } from "./pages/recoverPassword";
import { Home } from "./pages/home";
import { Stores } from "./pages/stores/List";
import { Graduates } from "./pages/graduates";
import { Sales } from "./pages/sales";
import { StoreForm } from "./pages/stores/Form";
import { Users } from "./pages/users/List";
import { UserForm } from "./pages/users/Form";
import { NewPassword } from "./pages/newPassword";
import { useLogin } from "./hooks/useLogin";
import { Navigate } from "react-router-dom";
import { ProfileTypeEnum } from "./pages/users/enums";
import { useCustomToast } from "./hooks/useToast";

export const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/login" Component={Login} />
      <Route path="/criar-conta" Component={NewAccount} />
      <Route path="/recuperar-senha" Component={ForgotPassword} />
      <Route path="/alterar-senha/:token" Component={NewPassword} />
      <Route
        path="/"
        element={
          <ProtectedRoute>
            <Home />
          </ProtectedRoute>
        }
      />
      <Route path="/lojas" Component={Stores} />
      <Route path="/loja/:storeId/formandos" Component={Graduates} />
      <Route path="/loja/:storeId/vendas" Component={Sales} />
      <Route path="/loja/cadastro" Component={StoreForm} />
      <Route path="/loja/editar/:id" Component={StoreForm} />
      <Route path="/usuarios" Component={Users} />
      <Route path="/usuario/cadastro" Component={UserForm} />
      <Route path="/usuario/editar/:id" Component={UserForm} />
    </Routes>
  );
};

const ProtectedRoute = ({ children }: any) => {
  const { isAuthenticated, getAccount } = useLogin();
  const { customToast } = useCustomToast();

  if (
    !isAuthenticated ||
    getAccount()?.profile !== ProfileTypeEnum.administrador
  ) {
    customToast("warning", "A conta não possui as permissões necessárias.");
    return <Navigate to="/login" replace />;
  }

  return children;
};
