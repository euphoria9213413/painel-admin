import { extendBaseTheme } from "@chakra-ui/react";

const customTheme = {
  components: {},
  colors: {
    primary: {
      500: "#62CD0F",
    },
    secondary: {
      100: "#fab9a2",
      300: "#fa936e",
      500: "#F15A22",
    },
    neutral: {
      0: "#FFFFFF",
      10: "#C6C6C6",
      20: "#B0B0B0",
      50: "#8C8C8C",
      100: "#F9F9F9",
      200: "#EAEAEA",
      300: "#BCBCBC",
      350: "#696969",
      400: "#353535",
      500: "#2F2F2F",
      600: "#363843",
      700: "#171717",
    },
    fail: {
      500: "#eb0909",
    },
  },
};

export const theme = extendBaseTheme(customTheme);
