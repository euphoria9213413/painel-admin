export const onlyNumbers = (value: string) => value.replace(/\D/g, "");

export function maskCPF(value: string) {
  return onlyNumbers(value).replace(
    /(\d{3})(\d{3})(\d{3})(\d{1,2})/,
    "$1.$2.$3-$4"
  );
}
