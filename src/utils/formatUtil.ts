import { KeyboardEvent } from "react";

export const priceFormat = (price: number) =>
  price.toLocaleString("pt-BR", { currency: "BRL" });

export const moneyFormat = (price: number) =>
  price.toLocaleString("pt-BR", { style: "currency", currency: "BRL" });

const VALID_NUMBERS = /^[0-9]$/;
const INVALID_CHARS = /\.|,|R|\$/g;
const VALID_KEYS = ["Backspace", "ArrowLeft", "ArrowRight", "Delete"];

export const validateInputCharacter = (
  event: KeyboardEvent<HTMLInputElement>
) => {
  const { key, target } = event;
  const treatedTarget = target as HTMLInputElement;
  const value = formatInputValueToNumber(treatedTarget.value);
  const isValidKey = VALID_NUMBERS.test(key) || VALID_KEYS.includes(key);
  const isFirstNumberZero = value === 0 && key === "0";
  if (!isValidKey || isFirstNumberZero) return event.preventDefault();
};

export const formatInputValueToNumber = (value: string): number => {
  const removedDotOrComma = value.replace(INVALID_CHARS, "");

  return Number(removedDotOrComma);
};
