export const dateStringToDateFormat = (dateString: string) => {
  const dateComponents = dateString.split("/");

  return new Date(
    Number(dateComponents[2]),
    Number(dateComponents[1]) - 1,
    Number(dateComponents[0])
  ).toISOString();
};

enum monthNames {
  "JAN",
  "FEV",
  "MAR",
  "ABR",
  "MAI",
  "JUN",
  "JUL",
  "AGO",
  "SET",
  "OUT",
  "NOV",
  "DEZ",
}

export const dateStringToShortMonthUtc = (date: string) => {
  const dateSplit = date.split("T");
  const formatedDate = dateSplit[0].split("-");

  return monthNames[Number(formatedDate[1]) - 1];
};

export const dateTimezoneISOString = (date: string) => {
  const tzoffset = new Date().getTimezoneOffset() * 60000; //offset in milliseconds
  return new Date((new Date(date) as any) - tzoffset)
    .toISOString()
    .slice(0, -1);
};

export const getDate = (date: string) => {
  const dateSplit = date.split("T");
  const formatedDate = dateSplit[0].split("-");
  return `${formatedDate[2]}/${formatedDate[1]}/${formatedDate[0]}`;
};

export const getHour = (date: string) => {
  const dateSplit = date.split("T");
  const formatedHour = dateSplit[1].split(":");
  return `${formatedHour[0]}:${formatedHour[1]}`;
};
