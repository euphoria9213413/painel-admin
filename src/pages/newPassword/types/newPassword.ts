export type UpdateAccountPasswordDto = {
  newPassword: string;
  newPasswordConfirmation: string;
};
