import {
  Badge,
  HStack,
  InputGroup,
  InputLeftElement,
  Progress,
  Select,
  Stack,
  Text,
  VStack,
} from "@chakra-ui/react";
import { MainContainer } from "../../../components/mainContainer";
import { FilterBar } from "../../../components/FilterBar";
import { SearchIcon } from "../../../components/icons";
import { DayMonthBox } from "../../../components/dayMonthBox";
import { MenuActionButton } from "../../../components/menuActionButton";
import { useCallback, useEffect, useState } from "react";
import { Pagination } from "../../../components/pagination";
import { MenuPopUp } from "../../../components/menuPopUp";
import * as Inputs from "../../../components/inputs";
import { useNavigate } from "react-router-dom";
import { StoreIndexDto, StoreIndexFilterDto } from "../types";
import { useStore } from "@/hooks/useStore";
import { priceFormat } from "@/utils/formatUtil";
import { dateStringToShortMonthUtc, getDate } from "@/utils/dateUtil";
import LoadingOverlay from "@/components/overlay";
import { useCustomToast } from "@/hooks/useToast";
import { env } from "@/config/env";

const initialFilter: StoreIndexFilterDto = {
  name: "",
  situation: "todos",
};

/* eslint-disable react/no-children-prop */
export const Stores = () => {
  const navigate = useNavigate();
  const { customToast } = useCustomToast();
  const [showMenuPopUp, setShowMenuPopUp] = useState<undefined | number>();
  const { getStores, storeSituationUpdate } = useStore();
  const [page, setPage] = useState<number>(1);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [stores, setStores] = useState<StoreIndexDto[]>([]);
  const [totalStores, setTotalStores] = useState<number>(0);
  const [lastPage, setLastPage] = useState<number>(1);
  const [filter, setFilter] = useState<StoreIndexFilterDto>(initialFilter);
  const [stopedTyping, setStopedTyping] = useState(false);
  const [typingTimer, setTypingTimer] = useState<NodeJS.Timeout | undefined>();

  const handleInputChange = () => {
    clearTimeout(typingTimer);

    const newTypingTimer = setTimeout(() => {
      setStopedTyping(true);
    }, 500);

    setTypingTimer(newTypingTimer);
  };

  const filters = [
    {
      field: (
        <InputGroup alignItems="center" key={1}>
          <InputLeftElement
            pointerEvents="none"
            children={<SearchIcon />}
            top=""
            left={1}
          />
          <Inputs.SimpleText
            placeholder="Buscar por nome"
            w="100%"
            h={9}
            borderRadius={4}
            textIndent="35px"
            bg="neutral.0"
            onChange={(event) => {
              setFilter({ ...filter, name: event.target.value });
              handleInputChange();
            }}
          />
        </InputGroup>
      ),
    },
    {
      field: (
        <HStack w={260} justifyContent="flex-end" key={2}>
          <Text
            w={100}
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="400"
            fontSize="14px"
            lineHeight="17px"
            color="neutral.500"
          >
            Filtrar por status:
          </Text>
          <Select
            width={110}
            h={9}
            borderRadius={4}
            bg="neutral.0"
            onChange={(event) =>
              setFilter({ ...filter, situation: event.target.value })
            }
          >
            <option value="">Todos</option>
            <option value="ativos">Ativos</option>
            <option value="inativos">Inativos</option>
          </Select>
        </HStack>
      ),
    },
  ];

  const menuPopUpOptions = (store: StoreIndexDto) => [
    {
      title: "Editar loja",
      onClick: () => navigate(`/loja/editar/${store.id}`),
    },
    {
      title: "Link da loja",
      onClick: () => {
        navigator.clipboard.writeText(
          `${env.react_app_ecommerce_uri}/evento/${store.slug}`
        );
        customToast("success", "Link da loja copiado com sucesso");
      },
    },
    {
      title: "Formandos",
      onClick: () => navigate(`/loja/${store.id}/formandos`),
    },
    { title: "Vendas", onClick: () => navigate(`/loja/${store.id}/vendas`) },
    {
      title: store.situation ? "Inativar" : "Ativar",
      onClick: () => {
        setIsLoading(true);
        storeSituationUpdate(store.id, !store.situation).finally(() =>
          callbackStores()
        );
      },
    },
  ];

  const callbackStores = useCallback(() => {
    setIsLoading(true);
    getStores({ page, ...filter })
      .then((response) => {
        setStores(response.data.data);
        setTotalStores(response.data.total);
        setLastPage(response.data.lastPage);
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  }, [page, filter]);

  useEffect(() => callbackStores(), [page, filter.situation]);
  useEffect(() => {
    if (stopedTyping) {
      if (page === 1) {
        callbackStores();
      } else {
        setPage(1);
      }
      setStopedTyping(false);
    }
  }, [filter.name, stopedTyping]);

  return (
    <MainContainer
      height={isLoading || stores.length < 10 ? undefined : "100%"}
    >
      <FilterBar
        title={`Lojas cadastradas (${totalStores})`}
        buttons={[
          { title: "Nova Loja", onClick: () => navigate("/loja/cadastro") },
        ]}
        filters={filters}
      />
      {isLoading && <LoadingOverlay />}
      {!isLoading && (
        <Stack w="100%" p={5} mt="0 !important">
          {stores.map((store) => (
            <HStack
              mt="3px !important"
              key={store.id}
              bgColor="neutral.0"
              w="100%"
              h="70px"
              borderRadius={3}
              pl={3}
              justifyContent="space-between"
              position="relative"
            >
              {store.raffleDate ? (
                <DayMonthBox
                  day={Number(getDate(store.raffleDate).split("/")[0])}
                  month={dateStringToShortMonthUtc(store.raffleDate)}
                />
              ) : (
                <VStack position="relative" w="45px" h="45px"></VStack>
              )}
              <Text
                fontFamily="Inter"
                fontStyle="normal"
                fontWeight="400"
                fontSize="15px"
                lineHeight="23px"
                color="neutral.500"
                w="15%"
                textAlign="center"
              >
                {store.name}
              </Text>
              <Text
                fontFamily="Inter"
                fontStyle="normal"
                fontWeight="400"
                fontSize="15px"
                lineHeight="23px"
                color="neutral.500"
                w="15%"
                textAlign="center"
              >
                {`De ${getDate(store.initialDate)} à ${getDate(
                  store.finalDate
                )}`}
              </Text>
              <VStack w="15%">
                <Text
                  fontFamily="Inter"
                  fontStyle="normal"
                  fontWeight="400"
                  fontSize="13px"
                  lineHeight="19px"
                  color="neutral.500"
                >
                  {`${priceFormat(store.goal.goalAchieved)} de ${priceFormat(
                    store.goal.goalAmount
                  )} | ${store.goal.goalPercent}`}
                </Text>
                <Progress
                  colorScheme="primary"
                  size="sm"
                  value={Number(store.goal.goalPercent.replace("%", ""))}
                  borderRadius={10}
                  w={48}
                  h={3}
                />
              </VStack>
              <HStack w="8%" justifyContent="space-between">
                <Text
                  fontFamily="Inter"
                  fontStyle="normal"
                  fontWeight="400"
                  fontSize="15px"
                  lineHeight="20px"
                  color="neutral.500"
                >
                  {store.situation ? "Ativo" : "inativo"}
                </Text>
                <Badge
                  bgColor={store.situation ? "primary.500" : "fail.500"}
                  w={5}
                  h={5}
                  borderRadius="50%"
                />
              </HStack>
              <MenuActionButton
                w="10%"
                color="#2F2F2F"
                onClick={() => setShowMenuPopUp(store.id)}
                _hover={{
                  bgColor: "neutral.200",
                }}
                _active={{
                  bgColor: "neutral.300",
                }}
              />
              {showMenuPopUp === store.id && (
                <MenuPopUp
                  top="-1px"
                  right="35px"
                  options={menuPopUpOptions(store)}
                  setShowMenuPopUp={setShowMenuPopUp}
                />
              )}
            </HStack>
          ))}
          {stores.length && (
            <Pagination page={page} lastPage={lastPage} onClick={setPage} />
          )}
        </Stack>
      )}
    </MainContainer>
  );
};
