import { FeePolicyType } from "../enums";

export type StoreIndexQueryDto = {
  name?: string;
  situation?: string;
  page: number;
};

type Goal = {
  goalAmount: number;
  goalAchieved: number;
  goalPercent: string;
};

export type StoreIndexDto = {
  id: number;
  name: string;
  slug: string;
  initialDate: string;
  finalDate: string;
  raffleDate: string;
  situation: boolean;
  goal: Goal;
};

export type StoreIndexPaginatedDto = {
  data: StoreIndexDto[];
  page: number;
  perPage: number;
  lastPage: number;
  total: number;
};

export type StoreIndexFilterDto = {
  name: string;
  situation: string;
};

export type StoreNameDto = {
  name: string;
};

export type ProductStoreDto = {
  id?: number;
  name: string;
  unitaryValue: string;
  graduateMargin: string;
  goalUnit: string;
  goalAmount: string;
};

export type StoreCreateDto = {
  name: string;
  slug: string;
  initialDate: string;
  finalDate: string;
  raffleDate?: string | null;
  situation: boolean | number;
  raffleUrl?: string;
  feePolicy: FeePolicyType;
  nickname?: string;
  desktopMedia?: File;
  mobileMedia?: File;
  videoUrl?: string;
  regulationUrl?: string;
  description?: string;
  products?: ProductStoreDto[];
};

export type MediaDataDto = {
  id: number;
  name: string;
  uniqueName: string;
  directory: string;
  mimeType: string;
  url: string;
  createdAt: string;
  updatedAt: string;
};

export type ProductDataDto = {
  id?: number;
  name: string;
  unitaryValue: string;
  graduateMargin: string;
  goalUnit: string;
  goalAmount: string;
  store?: string;
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
};

export type StoreDataDto = {
  id: number;
  name: string;
  slug: string;
  initialDate: string;
  finalDate: string;
  raffleDate: string;
  situation: boolean;
  raffleUrl: string;
  feePolicy: FeePolicyType;
  nickname: string;
  videoUrl: string;
  regulationUrl: string;
  description: string;
  desktopMedia: MediaDataDto;
  mobileMedia: MediaDataDto;
  products: ProductDataDto[];
  createdAt: string;
  updatedAt: string;
  deletedAt: string;
};
