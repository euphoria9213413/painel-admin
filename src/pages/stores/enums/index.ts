export enum FeePolicyEnum {
  absorver = "absorver",
  repassar = "repassar",
}

export type FeePolicyType = keyof typeof FeePolicyEnum;
