import {
  Box,
  Button,
  HStack,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import { MainContainer } from "../../../components/mainContainer";
import { BaseForms } from "./BaseForms";
import { ProductList } from "./ProductList";
import * as Fonts from "../../../components/texts";
import { useNavigate, useParams } from "react-router-dom";
import { useStore } from "@/hooks/useStore";
import { joiResolver } from "@hookform/resolvers/joi";
import { ProductDataDto, ProductStoreDto, StoreCreateDto } from "../types";
import { SubmitHandler, useForm } from "react-hook-form";
import Joi from "joi";
import { useEffect, useState } from "react";
import LoadingOverlay from "@/components/overlay";
import { useCustomToast } from "@/hooks/useToast";
import { ProductModal } from "./ProductList/ProductModal";
import { formatInputValueToNumber } from "@/utils/formatUtil";
import { dateTimezoneISOString } from "@/utils/dateUtil";

const schema = Joi.object({
  name: Joi.string().required().messages({
    "string.empty": "Nome deve ser informado",
  }),
  slug: Joi.string().required().messages({
    "string.empty": "Slug deve ser informado",
  }),
  initialDate: Joi.date().required().messages({
    "date.base": "Data inicial deve ser informada",
  }),
  finalDate: Joi.date().required().greater(Joi.ref("initialDate")).messages({
    "date.base": "Data final deve ser informada",
    "date.greater": "Data final deve ser maior que Data inicial",
  }),
  raffleDate: Joi.date().allow("", null),
  situation: Joi.number().required().messages({
    "number.empty": "Situação deve ser informada",
  }),
  raffleUrl: Joi.string().allow("", null),
  feePolicy: Joi.string().required().messages({
    "string.empty": "Política de taxas deve ser informada",
  }),
  nickname: Joi.string().allow("", null),
  desktopMedia: Joi.object().allow("", null),
  mobileMedia: Joi.object().allow("", null),
  videoUrl: Joi.string().allow("", null),
  regulationUrl: Joi.string().allow("", null),
  description: Joi.string().allow("", null),
  products: Joi.array()
    .items(
      Joi.object({
        id: Joi.number().allow(null),
        name: Joi.string().required().messages({
          "string.empty": "Política de taxas deve ser informada",
        }),
        unitaryValue: Joi.string().required().messages({
          "string.empty": "Política de taxas deve ser informada",
        }),
        graduateMargin: Joi.string().allow(0, null),
        goalUnit: Joi.string().allow(0, null),
        goalAmount: Joi.string().required().messages({
          "string.empty": "Política de taxas deve ser informada",
        }),
      })
    )
    .required(),
});

const initialValue: StoreCreateDto = {
  name: "",
  slug: "",
  initialDate: "",
  finalDate: "",
  situation: 0,
  feePolicy: "absorver",
  products: [],
};

export const StoreForm = () => {
  const navigate = useNavigate();
  const { id: storeId } = useParams();
  const { customToast } = useCustomToast();
  const {
    getStore,
    storeCreate,
    storeUpdate,
    storeMediasUpload,
    productUpdate,
    productCreate,
    productDelete,
  } = useStore();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [defaultValues, setDefaultValues] =
    useState<StoreCreateDto>(initialValue);
  const [products, setProducts] = useState<ProductDataDto[]>([]);
  const [productEditIndex, setProductEditIndex] = useState<
    number | undefined
  >();
  const [productToEdit, setProductToEdit] = useState<
    ProductDataDto | undefined
  >();
  const [desktopMediaName, setDesktopMediaName] = useState<string>("");
  const [mobileMediaName, setMobileMediaName] = useState<string>("");

  useEffect(() => {
    if (!storeId) return;

    setIsLoading(true);

    getStore(Number(storeId))
      .then((response) => {
        const data = {
          ...response.data,
          situation: response.data.situation ? 1 : 0,
          initialDate: response.data.initialDate
            .replace("T", " ")
            .split(".")[0],
          finalDate: response.data.finalDate.replace("T", " ").split(".")[0],
          raffleDate: response.data.raffleDate
            ? response.data.raffleDate.replace("T", " ").split(".")[0]
            : "",
        };

        /* eslint-disable @typescript-eslint/no-unused-vars */
        const {
          id,
          createdAt,
          updatedAt,
          deletedAt,
          desktopMedia,
          mobileMedia,
          ...result
        } = data;

        const products = data.products.map((product) => {
          const { store, createdAt, updatedAt, deletedAt, ...data } = product;

          return { ...data, goalUnit: `${data.goalUnit}` };
        });

        setProducts(products);
        setDefaultValues({ ...result, products });
        setDesktopMediaName(response.data.desktopMedia.name);
        setMobileMediaName(response.data.mobileMedia.name);
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  }, []);

  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues]);

  const {
    register,
    handleSubmit,
    watch,
    setValue,
    reset,
    clearErrors,
    formState: { errors },
  } = useForm<StoreCreateDto>({
    resolver: joiResolver(schema),
  });

  const desktopMedia = watch("desktopMedia");
  const mobileMedia = watch("mobileMedia");

  useEffect(() => {
    if (desktopMedia) setDesktopMediaName(desktopMedia.name);
    if (mobileMedia) setMobileMediaName(mobileMedia.name);
  }, [desktopMedia, mobileMedia]);

  const onSubmit: SubmitHandler<StoreCreateDto> = (props) => {
    setIsLoading(true);

    const { desktopMedia, mobileMedia, ...data } = props;
    data.situation = props.situation ? true : false;
    data.initialDate = dateTimezoneISOString(
      new Date(props.initialDate).toISOString()
    );
    data.finalDate = dateTimezoneISOString(
      new Date(props.finalDate).toISOString()
    );
    data.raffleDate = props.raffleDate
      ? dateTimezoneISOString(new Date(props.raffleDate).toISOString())
      : null;

    if (storeId) delete data.products;

    const request = storeId
      ? storeUpdate(Number(storeId), data)
      : storeCreate(data);
    request
      .then((response) => {
        if (!props.desktopMedia && !props.mobileMedia) {
          customToast(
            "success",
            storeId ? "Atualizado com sucesso" : "Cadastrado com sucesso"
          );
          setIsLoading(false);
          navigate("/lojas");
          return;
        }

        storeMediasUpload(
          response.data.id || Number(storeId),
          desktopMedia,
          mobileMedia
        )
          .then(() => {
            customToast(
              "success",
              storeId ? "Atualizado com sucesso" : "Cadastrado com sucesso"
            );
            navigate("/lojas");
          })
          .catch((error) => {
            customToast("error", [error.response.data.message].join(", "));
            navigate(`/loja/editar/${response.data.id || Number(storeId)}`);
          })
          .finally(() => {
            setIsLoading(false);
          });
      })
      .catch((error) => {
        customToast("error", [error.response.data.message].join(", "));
        setIsLoading(false);
      });
  };

  const onProductUpdate = (index: number) => {
    setProductEditIndex(index);

    const product = products[index];
    setProductToEdit(product);

    onOpen();
  };

  const onProductSubmit = (data: ProductStoreDto) => {
    const newData: ProductStoreDto = {
      ...data,
      unitaryValue: `${formatInputValueToNumber(data.unitaryValue) / 100}`,
      graduateMargin: `${formatInputValueToNumber(data.graduateMargin) / 100}`,
      goalUnit: `${formatInputValueToNumber(data.goalUnit)}`,
      goalAmount: `${formatInputValueToNumber(data.goalAmount) / 100}`,
    };

    if (!storeId) {
      if (productToEdit) {
        const newProducts = [
          ...products.slice(0, productEditIndex),
          newData,
          ...products.slice(productEditIndex! + 1),
        ];

        setProducts(newProducts);
        setValue("products", newProducts);
        setProductEditIndex(undefined);
        setProductToEdit(undefined);
        return;
      }

      setProducts([...products, newData]);
      setValue("products", [...products, newData]);
      return;
    }

    setIsLoading(true);

    if (productToEdit) {
      productUpdate(productToEdit.id!, newData)
        .then((response) => {
          const { store, createdAt, updatedAt, deletedAt, ...data } =
            response.data;

          const newProducts = [
            ...products.slice(0, productEditIndex),
            data,
            ...products.slice(productEditIndex! + 1),
          ];

          customToast("success", "Produto atualizado com sucesso");
          setProducts(newProducts);
          setProductEditIndex(undefined);
          setProductToEdit(undefined);
        })
        .catch((error) => {
          customToast("error", [error.response.data.message].join(", "));
        })
        .finally(() => setIsLoading(false));

      return;
    }

    productCreate(Number(storeId), newData)
      .then((response) => {
        const { store, createdAt, updatedAt, deletedAt, ...data } =
          response.data;

        setProducts([...products, data]);
        customToast("success", "Produto cadastrado com sucesso");
      })
      .catch((error) => {
        customToast("error", [error.response.data.message].join(", "));
      })
      .finally(() => setIsLoading(false));
  };

  const onProductDelete = (index: number) => {
    if (!storeId) {
      const newProducts = [
        ...products.slice(0, index),
        ...products.slice(index! + 1),
      ];
      setProducts(newProducts);
      setValue("products", newProducts);
      return;
    }

    setIsLoading(true);

    productDelete(Number(products[index].id))
      .then(() => {
        const newProducts = [
          ...products.slice(0, index),
          ...products.slice(index! + 1),
        ];
        setProducts(newProducts);
        setValue("products", newProducts);
      })
      .catch((error) => {
        customToast("error", [error.response.data.message].join(", "));
      })
      .finally(() => setIsLoading(false));
  };

  return (
    <MainContainer height="100%" title="Painel de controle > Nova Loja">
      {isLoading && <LoadingOverlay />}
      <Stack w="100%" p={5}>
        <Text
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="600"
          fontSize="24px"
          lineHeight="31px"
          color="neutral.600"
        >
          Cadastro de Nova Loja
        </Text>
        <Box
          bgColor="neutral.100"
          borderRadius={5}
          p={3}
          as="form"
          onSubmit={handleSubmit(onSubmit)}
        >
          <BaseForms
            register={register}
            errors={errors}
            setValue={setValue}
            desktopMediaName={desktopMediaName}
            mobileMediaName={mobileMediaName}
            watch={watch}
            clearErrors={clearErrors}
          />
          <ProductList
            onOpen={onOpen}
            products={products}
            onProductUpdate={onProductUpdate}
            onProductDelete={onProductDelete}
          />
          <HStack spacing={10} justifyContent="center" mb={50}>
            <Button
              variant="outline"
              borderColor="secondary.500"
              w={180}
              h={9}
              borderRadius={4}
              onClick={() => navigate("/lojas")}
              _hover={{
                bgColor: "secondary.100",
              }}
              _active={{
                bgColor: "secondary.300",
              }}
            >
              <Fonts.ButtonText color="secondary.500">
                Cancelar
              </Fonts.ButtonText>
            </Button>
            <Button
              variant="solid"
              type="submit"
              bgColor="secondary.500"
              w={180}
              h={9}
              borderRadius={4}
              _hover={{
                bgColor: "secondary.300",
              }}
              _active={{
                bgColor: "secondary.500",
              }}
            >
              <Fonts.ButtonText>Salvar Alterações</Fonts.ButtonText>
            </Button>
          </HStack>
        </Box>
        <ProductModal
          isOpen={isOpen}
          onClose={onClose}
          onSubmit={onProductSubmit}
          product={productToEdit}
          setProductEditIndex={setProductEditIndex}
          setProductToEdit={setProductToEdit}
        />
      </Stack>
    </MainContainer>
  );
};
