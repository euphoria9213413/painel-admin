import {
  Box,
  Button,
  FormControl,
  HStack,
  Input,
  InputGroup,
  InputLeftAddon,
  Select,
  Text,
  VStack,
} from "@chakra-ui/react";
import * as Fonts from "../../../components/texts";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { ChangeEvent, useEffect, useRef, useState } from "react";
import {
  FieldErrors,
  UseFormClearErrors,
  UseFormRegister,
  UseFormSetValue,
  UseFormWatch,
} from "react-hook-form";
import { StoreCreateDto } from "../types";
import { FeePolicyEnum } from "../enums";
import { env } from "@/config/env";
import { dateTimezoneISOString } from "@/utils/dateUtil";

// const modules = {
//   toolbar: [
//     [{ header: [1, 2, 3, 4, 5, 6, false] }],
//     [{ font: [] }],
//     [{ size: [] }],
//     ["bold", "italic"],
//     [{ list: "ordered" }],
//   ],
// };

type Props = {
  register: UseFormRegister<StoreCreateDto>;
  errors: FieldErrors<StoreCreateDto>;
  setValue: UseFormSetValue<StoreCreateDto>;
  desktopMediaName: string;
  mobileMediaName: string;
  watch: UseFormWatch<StoreCreateDto>;
  clearErrors: UseFormClearErrors<StoreCreateDto>;
};

/* eslint-disable react/no-children-prop */
export const BaseForms = ({
  register,
  errors,
  setValue,
  desktopMediaName,
  mobileMediaName,
  watch,
  clearErrors,
}: Props) => {
  const [initialDateState, setInitialDateState] = useState<
    string | undefined
  >();
  const [finalDateState, setFinalDateState] = useState<string | undefined>();
  const [raffleDateState, setRaffleDateState] = useState<string | undefined>();

  const inputDesktopMediaFile = useRef<any>(null);
  const inputMobileMediaFile = useRef<any>(null);

  const initialDate = watch("initialDate");
  const finalDate = watch("finalDate");
  const raffleDate = watch("raffleDate");
  const description = watch("description");

  useEffect(() => {
    setInitialDateState(
      initialDate
        ? dateTimezoneISOString(initialDate.split(".")[0]).replace("T", " ")
        : undefined
    );
    setFinalDateState(
      finalDate
        ? dateTimezoneISOString(finalDate.split(".")[0]).replace("T", " ")
        : undefined
    );
    setRaffleDateState(
      raffleDate
        ? dateTimezoneISOString(raffleDate.split(".")[0]).replace("T", " ")
        : undefined
    );
  }, [initialDate, finalDate, raffleDate]);

  return (
    <>
      <HStack w="100%" spacing={5} mt={5}>
        <FormControl w="100%">
          <Fonts.FormLabelText>Nome da loja*</Fonts.FormLabelText>
          <Input
            type="text"
            textIndent={"1px"}
            w="100%"
            h={10}
            borderRadius={3}
            border="1px solid #EAEAEA"
            bgColor="neutral.0"
            {...register("name")}
          />
          {errors?.name?.message && (
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="500"
              fontSize="11px"
              lineHeight="18px"
              color="fail.500"
            >
              {errors.name.message}
            </Text>
          )}
        </FormControl>
        <FormControl w="100%">
          <Fonts.FormLabelText>Slug*</Fonts.FormLabelText>
          <InputGroup alignItems="center">
            <InputLeftAddon
              h={10}
              bgColor="neutral.200"
              borderRadius={3}
              children={`${env.react_app_ecommerce_uri}/evento/`}
              pl={2}
              pr={2}
            />
            <Input
              type="text"
              textIndent={"1px"}
              w="100%"
              h={10}
              borderRadius={3}
              border="1px solid #EAEAEA"
              bgColor="neutral.0"
              {...register("slug")}
            />
          </InputGroup>
          {errors?.slug?.message && (
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="500"
              fontSize="11px"
              lineHeight="18px"
              color="fail.500"
            >
              {errors.slug.message}
            </Text>
          )}
        </FormControl>
      </HStack>
      <HStack w="100%" spacing={5} mt={6}>
        <FormControl w="100%">
          <Fonts.FormLabelText>Data inicial*</Fonts.FormLabelText>
          <Input
            type="datetime-local"
            textIndent={"1px"}
            w="100%"
            h={10}
            borderRadius={3}
            border="1px solid #EAEAEA"
            bgColor="neutral.0"
            value={initialDateState}
            onChange={(event) => {
              clearErrors("initialDate");
              setValue("initialDate", event.target.value);
            }}
          />
          {errors?.initialDate?.message && (
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="500"
              fontSize="11px"
              lineHeight="18px"
              color="fail.500"
            >
              {errors.initialDate.message}
            </Text>
          )}
        </FormControl>
        <FormControl w="100%">
          <Fonts.FormLabelText>Data final*</Fonts.FormLabelText>
          <Input
            type="datetime-local"
            textIndent={"1px"}
            w="100%"
            h={10}
            borderRadius={3}
            border="1px solid #EAEAEA"
            bgColor="neutral.0"
            value={finalDateState}
            onChange={(event) => {
              clearErrors("finalDate");
              setValue("finalDate", event.target.value);
            }}
          />
          {errors?.finalDate?.message && (
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="500"
              fontSize="11px"
              lineHeight="18px"
              color="fail.500"
            >
              {errors.finalDate.message}
            </Text>
          )}
        </FormControl>
        <FormControl w="100%">
          <Fonts.FormLabelText>Data de sorteio</Fonts.FormLabelText>
          <Input
            type="datetime-local"
            textIndent={"1px"}
            w="100%"
            h={10}
            borderRadius={3}
            border="1px solid #EAEAEA"
            bgColor="neutral.0"
            value={raffleDateState}
            onChange={(event) => {
              clearErrors("raffleDate");
              setValue("raffleDate", event.target.value);
            }}
          />
          {errors?.raffleDate?.message && (
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="500"
              fontSize="11px"
              lineHeight="18px"
              color="fail.500"
            >
              {errors.raffleDate.message}
            </Text>
          )}
        </FormControl>
        <FormControl w="100%">
          <Fonts.FormLabelText>Situação*</Fonts.FormLabelText>
          <Select
            h={10}
            borderRadius={4}
            bgColor="neutral.0"
            {...register("situation")}
          >
            <option value={1}>Ativo</option>
            <option value={0}>Inativo</option>
          </Select>
          {errors?.situation?.message && (
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="500"
              fontSize="11px"
              lineHeight="18px"
              color="fail.500"
            >
              {errors.situation.message}
            </Text>
          )}
        </FormControl>
      </HStack>
      <HStack w="100%" spacing={5} mt={6}>
        <FormControl w="50%">
          <Fonts.FormLabelText>Link do sorteio</Fonts.FormLabelText>
          <Input
            type="text"
            textIndent={"1px"}
            w="100%"
            h={10}
            borderRadius={3}
            border="1px solid #EAEAEA"
            bgColor="neutral.0"
            {...register("raffleUrl")}
          />
          {errors?.raffleUrl?.message && (
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="500"
              fontSize="11px"
              lineHeight="18px"
              color="fail.500"
            >
              {errors.raffleUrl.message}
            </Text>
          )}
        </FormControl>
        <FormControl w="25%">
          <Fonts.FormLabelText>Política de taxas*</Fonts.FormLabelText>
          <Select
            h={10}
            borderRadius={4}
            bgColor="neutral.0"
            {...register("feePolicy")}
          >
            <option value={FeePolicyEnum.absorver}>Absorver</option>
            <option value={FeePolicyEnum.repassar}>Repassar</option>
          </Select>
          {errors?.feePolicy?.message && (
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="500"
              fontSize="11px"
              lineHeight="18px"
              color="fail.500"
            >
              {errors.feePolicy.message}
            </Text>
          )}
        </FormControl>
        <FormControl w="25%">
          <Fonts.FormLabelText>Apelido da loja</Fonts.FormLabelText>
          <Input
            type="text"
            textIndent={"1px"}
            w="100%"
            h={10}
            borderRadius={3}
            border="1px solid #EAEAEA"
            bgColor="neutral.0"
            {...register("nickname")}
          />
          {errors?.nickname?.message && (
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="500"
              fontSize="11px"
              lineHeight="18px"
              color="fail.500"
            >
              {errors.nickname.message}
            </Text>
          )}
        </FormControl>
      </HStack>
      <HStack w="100%" spacing={5} mt={6} alignItems="flex-start">
        <VStack w="33%" alignItems="flex-start">
          <HStack w="100%" alignItems="flex-end">
            <FormControl w="100%">
              <Fonts.FormLabelText>Banner desktop</Fonts.FormLabelText>
              <input
                ref={inputDesktopMediaFile}
                type="file"
                style={{ display: "none" }}
                onChange={(event: ChangeEvent<HTMLInputElement>) => {
                  if (event.target.files) {
                    setValue("desktopMedia", event.target.files[0]);
                  }
                }}
              />
              <Input
                type="text"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                onClick={() => inputDesktopMediaFile.current.click()}
                value={desktopMediaName}
              />
              {errors?.desktopMedia?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.desktopMedia.message}
                </Text>
              )}
            </FormControl>
            <Button
              h={10}
              bgColor="secondary.500"
              borderRadius={3}
              marginStart="0 !important"
              onClick={() => inputDesktopMediaFile.current.click()}
              variant="solid"
              _hover={{
                bgColor: "secondary.300",
              }}
              _active={{
                bgColor: "secondary.500",
              }}
            >
              <Text
                fontFamily="Inter"
                fontStyle="normal"
                fontWeight="700"
                fontSize="14px"
                lineHeight="17px"
                color="neutral.0"
                pl={8}
                pr={8}
              >
                Buscar
              </Text>
            </Button>
          </HStack>
          <Fonts.FormInputInformationText>
            Dimensão recomendada: 800 x 350
          </Fonts.FormInputInformationText>
        </VStack>
        <VStack w="33%" alignItems="flex-start">
          <HStack w="100%" alignItems="flex-end">
            <FormControl w="100%">
              <Fonts.FormLabelText>Banner mobile</Fonts.FormLabelText>
              <input
                ref={inputMobileMediaFile}
                type="file"
                style={{ display: "none" }}
                onChange={(event: ChangeEvent<HTMLInputElement>) => {
                  if (event.target.files) {
                    setValue("mobileMedia", event.target.files[0]);
                  }
                }}
              />
              <Input
                type="text"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                onClick={() => inputMobileMediaFile.current.click()}
                value={mobileMediaName}
              />
              {errors?.mobileMedia?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.mobileMedia.message}
                </Text>
              )}
            </FormControl>
            <Button
              h={10}
              bgColor="secondary.500"
              borderRadius={3}
              marginStart="0 !important"
              onClick={() => inputMobileMediaFile.current.click()}
              variant="solid"
              _hover={{
                bgColor: "secondary.300",
              }}
              _active={{
                bgColor: "secondary.500",
              }}
            >
              <Text
                fontFamily="Inter"
                fontStyle="normal"
                fontWeight="700"
                fontSize="14px"
                lineHeight="17px"
                color="neutral.0"
                pl={8}
                pr={8}
              >
                Buscar
              </Text>
            </Button>
          </HStack>
          <Fonts.FormInputInformationText>
            Dimensão recomendada: 300 x 300
          </Fonts.FormInputInformationText>
        </VStack>
        <VStack w="33%" alignItems="flex-start">
          <HStack w="100%" alignItems="flex-end">
            <FormControl w="100%">
              <Fonts.FormLabelText>URL vídeo</Fonts.FormLabelText>
              <Input
                type="text"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                {...register("videoUrl")}
              />
              {errors?.videoUrl?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.videoUrl.message}
                </Text>
              )}
            </FormControl>
          </HStack>
        </VStack>
      </HStack>
      <HStack w="100%" spacing={5} mt={6} pr={6} alignItems="flex-start">
        <VStack w="33%" alignItems="flex-start">
          <HStack w="100%" alignItems="flex-end">
            <FormControl w="100%">
              <Fonts.FormLabelText>Link do regulamento</Fonts.FormLabelText>
              <Input
                type="text"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                {...register("regulationUrl")}
              />
              {errors?.regulationUrl?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.regulationUrl.message}
                </Text>
              )}
            </FormControl>
          </HStack>
          <Fonts.FormInputInformationText>
            Arquivo público
          </Fonts.FormInputInformationText>
        </VStack>
      </HStack>
      <Box mt={6}>
        <Fonts.FormLabelText>Descrição</Fonts.FormLabelText>
        <CKEditor
          editor={ClassicEditor}
          config={{
            toolbar: [
              // {
              //   label: "Basic styles",
              //   icon: "text",
              //   items: ["bold", "italic"],
              // },
              "bold",
              "italic",
              "|",
              "numberedList",
              "bulletedList",
              "|",
              "blockQuote",
              "|",
              "selectAll",
              "|",
              "undo",
              "redo",
              "|",
              "plus",
              "text",
              "importExport",
              "alignLeft",
              "paragraph",
              "threeVerticalDots",
              // "decreaseIndent",
              // "increaseIndent",
              // "typing",
              // "clipboard",
              // "shiftEnter",
              "underLine",
            ],
          }}
          // data="<p>Hello from CKEditor 5!</p>"
          data={description || ""}
          // onReady={(editor) => {
          //   // You can store the "editor" and use when it is needed.
          //   console.log("Editor is ready to use!", editor);
          // }}
          onChange={(event, editor) => {
            const data = editor.getData();
            console.log({ event, editor, data });
            setValue("description", data);
          }}
          onBlur={(event, editor) => {
            console.log("Blur.", editor);
          }}
          // onFocus={(event, editor) => {
          //   console.log("Focus.", editor);
          // }}
        />
      </Box>
    </>
  );
};
