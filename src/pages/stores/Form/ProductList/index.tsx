import {
  Box,
  Button,
  HStack,
  Stack,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Tfoot,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import * as Font from "../../../../components/texts";
import * as T from "../../../../components/table";
import { MenuActionButton } from "../../../../components/menuActionButton";
import { useState } from "react";
import { MenuPopUp } from "../../../../components/menuPopUp";
import { NewIcon } from "@/components/icons";
import { ProductDataDto } from "../../types";
import { moneyFormat, priceFormat } from "@/utils/formatUtil";

type Props = {
  products: ProductDataDto[];
  onOpen: () => void;
  onProductUpdate: (index: number) => void;
  onProductDelete: (index: number) => void;
};

export const ProductList = ({
  products,
  onOpen,
  onProductUpdate,
  onProductDelete,
}: Props) => {
  const [showMenuPopUp, setShowMenuPopUp] = useState<undefined | number>();

  return (
    <Box mt={6}>
      <HStack justifyContent="space-between">
        <Text
          fontFamily="Inter"
          fontStyle="normal"
          fontWeight="600"
          fontSize="18px"
          lineHeight="22px"
          color="neutral.400"
        >
          Produtos
        </Text>
        <Button
          leftIcon={<NewIcon />}
          bgColor="secondary.500"
          w={180}
          h={9}
          borderRadius={4}
          onClick={onOpen}
          variant="solid"
          _hover={{
            bgColor: "secondary.300",
          }}
          _active={{
            bgColor: "secondary.500",
          }}
        >
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="600"
            fontSize="13px"
            lineHeight="18px"
            color="neutral.0"
          >
            Novo produto
          </Text>
        </Button>
      </HStack>
      <Stack w="100%" p={1} mt="0 !important">
        <TableContainer>
          <Table w="100%">
            <Thead>
              <Tr>
                <Th textAlign="center">
                  <Font.TableTheadText>Nome</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Valor un.</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Margem</Font.TableTheadText>
                  <Font.TableTheadText>Formando</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Meta (un.)</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Meta (R$)</Font.TableTheadText>
                </Th>
                <Th></Th>
              </Tr>
            </Thead>
            <Tbody>
              {products?.map((product, index) => (
                <T.Body key={product.id || Math.random()} hideBorderTop>
                  <Td textAlign="center">
                    <Font.TableBodyText>{product.name}</Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>
                      {moneyFormat(Number(product.unitaryValue))}
                    </Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>
                      {moneyFormat(Number(product.graduateMargin))}
                    </Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>
                      {priceFormat(Number(product.goalUnit))}
                    </Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>
                      {moneyFormat(Number(product.goalAmount))}
                    </Font.TableBodyText>
                  </Td>
                  <Td>
                    <MenuActionButton
                      color="#2F2F2F"
                      onClick={() => setShowMenuPopUp(index)}
                      _hover={{
                        bgColor: "neutral.200",
                      }}
                      _active={{
                        bgColor: "neutral.300",
                      }}
                    />
                  </Td>
                  {showMenuPopUp === index && (
                    <MenuPopUp
                      top="1px"
                      right="36px"
                      options={[
                        {
                          title: "Editar",
                          onClick: () => onProductUpdate(index),
                        },
                        {
                          title: "Excluir",
                          onClick: () => onProductDelete(index),
                        },
                      ]}
                      setShowMenuPopUp={setShowMenuPopUp}
                    />
                  )}
                </T.Body>
              ))}
            </Tbody>
            <Tfoot w="100%" h="40px"></Tfoot>
          </Table>
        </TableContainer>
      </Stack>
    </Box>
  );
};
