import {
  Button,
  FormControl,
  HStack,
  Input,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  Text,
} from "@chakra-ui/react";
import * as Fonts from "../../../../../components/texts";
import Joi from "joi";
import { SubmitHandler, useForm } from "react-hook-form";
import { ProductDataDto, ProductStoreDto } from "@/pages/stores/types";
import { joiResolver } from "@hookform/resolvers/joi";
import { useEffect } from "react";
import {
  formatInputValueToNumber,
  moneyFormat,
  priceFormat,
  validateInputCharacter,
} from "@/utils/formatUtil";

type Props = {
  isOpen: boolean;
  onClose: () => void;
  onSubmit: (data: ProductStoreDto) => void;
  product?: ProductStoreDto;
  setProductEditIndex: (value: number | undefined) => void;
  setProductToEdit: (value: ProductDataDto | undefined) => void;
};

const schema = Joi.object({
  id: Joi.number().allow(null),
  name: Joi.string().required().messages({
    "string.empty": "Nome deve ser informado",
  }),
  unitaryValue: Joi.string().required().messages({
    "string.empty": "Valor unitário deve ser informado",
  }),
  graduateMargin: Joi.string().required().messages({
    "string.empty": "Margem formando deve ser informada",
  }),
  goalUnit: Joi.string().required().messages({
    "string.empty": "Meta unidade deve ser informada",
  }),
  goalAmount: Joi.string().required().messages({
    "string.empty": "Meta (R$) deve ser informada",
  }),
});

/* eslint-disable react/no-children-prop */
export const ProductModal = ({
  isOpen,
  onClose,
  onSubmit,
  product,
  setProductEditIndex,
  setProductToEdit,
}: Props) => {
  const {
    register,
    handleSubmit,
    reset,
    watch,
    setValue,
    formState: { errors },
  } = useForm<ProductStoreDto>({
    resolver: joiResolver(schema),
  });

  const unitaryValue = watch("unitaryValue");
  const graduateMargin = watch("graduateMargin");
  const goalUnit = watch("goalUnit");
  const goalAmount = watch("goalAmount");

  useEffect(() => {
    reset({});
  }, [isOpen]);

  useEffect(() => {
    if (product) {
      return reset(product);
    }

    reset({});
  }, [product]);

  const handleClose = () => {
    setProductEditIndex(undefined);
    setProductToEdit(undefined);
    onClose();
  };

  const submitProduct: SubmitHandler<ProductStoreDto> = (data) => {
    reset();
    onSubmit(data);
    onClose();
  };

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      isCentered
      closeOnEsc={false}
      closeOnOverlayClick={false}
    >
      <ModalOverlay />
      <ModalContent
        left={142}
        bgColor="neutral.100"
        as="form"
        onSubmit={handleSubmit(submitProduct)}
      >
        <ModalHeader>Cadastro de Produto</ModalHeader>
        <ModalBody>
          <Stack spacing={5}>
            <FormControl w="100%">
              <Fonts.FormLabelText>Nome</Fonts.FormLabelText>
              <Input
                type="text"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                {...register("name")}
              />
              {errors.name?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.name.message}
                </Text>
              )}
            </FormControl>
            <FormControl w="100%">
              <Fonts.FormLabelText>Valor unitário</Fonts.FormLabelText>
              <Input
                type="text"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                {...register("unitaryValue")}
                onKeyDown={validateInputCharacter}
                onInput={(event) => {
                  const treatedTarget = event.target as HTMLInputElement;
                  const value = moneyFormat(
                    formatInputValueToNumber(treatedTarget.value) / 100
                  );
                  setValue("unitaryValue", value);
                }}
                value={
                  unitaryValue
                    ? moneyFormat(formatInputValueToNumber(unitaryValue) / 100)
                    : ""
                }
              />
              {errors?.unitaryValue?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.unitaryValue.message}
                </Text>
              )}
            </FormControl>
            <FormControl w="100%">
              <Fonts.FormLabelText>Margem formando</Fonts.FormLabelText>
              <Input
                type="text"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                {...register("graduateMargin")}
                onKeyDown={validateInputCharacter}
                onInput={(event) => {
                  const treatedTarget = event.target as HTMLInputElement;
                  const value = moneyFormat(
                    formatInputValueToNumber(treatedTarget.value) / 100
                  );
                  setValue("graduateMargin", value);
                }}
                value={
                  graduateMargin
                    ? moneyFormat(
                        formatInputValueToNumber(graduateMargin) / 100
                      )
                    : ""
                }
              />
              {errors?.graduateMargin?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.graduateMargin.message}
                </Text>
              )}
            </FormControl>
            <FormControl w="100%">
              <Fonts.FormLabelText>Meta unidade</Fonts.FormLabelText>
              <Input
                type="text"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                {...register("goalUnit")}
                onKeyDown={validateInputCharacter}
                onInput={(event) => {
                  const treatedTarget = event.target as HTMLInputElement;
                  const value = priceFormat(
                    formatInputValueToNumber(treatedTarget.value)
                  );
                  setValue("goalUnit", value);
                }}
                value={
                  goalUnit
                    ? priceFormat(
                        Number(
                          `${goalUnit}`.replaceAll(".", "").replaceAll(",", "")
                        )
                      )
                    : ""
                }
              />
              {errors?.goalUnit?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.goalUnit.message}
                </Text>
              )}
            </FormControl>
            <FormControl w="100%">
              <Fonts.FormLabelText>Meta (R$)</Fonts.FormLabelText>
              <Input
                type="text"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                {...register("goalAmount")}
                onKeyDown={validateInputCharacter}
                onInput={(event) => {
                  const treatedTarget = event.target as HTMLInputElement;
                  const value = moneyFormat(
                    formatInputValueToNumber(treatedTarget.value) / 100
                  );
                  setValue("goalAmount", value);
                }}
                value={
                  goalAmount
                    ? moneyFormat(formatInputValueToNumber(goalAmount) / 100)
                    : ""
                }
              />
              {errors?.goalAmount?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.goalAmount.message}
                </Text>
              )}
            </FormControl>
          </Stack>
        </ModalBody>
        <ModalFooter>
          <HStack w="100%" spacing={7}>
            <Button
              variant="outline"
              borderColor="secondary.500"
              colorScheme="secondary.500"
              w="100%"
              h={9}
              borderRadius={4}
              onClick={handleClose}
              _hover={{
                bgColor: "secondary.100",
              }}
              _active={{
                bgColor: "secondary.300",
              }}
            >
              <Fonts.ButtonText color="secondary.500">
                Cancelar
              </Fonts.ButtonText>
            </Button>
            <Button
              variant="solid"
              type="submit"
              bgColor="secondary.500"
              w="100%"
              h={9}
              borderRadius={4}
              _hover={{
                bgColor: "secondary.300",
              }}
              _active={{
                bgColor: "secondary.500",
              }}
            >
              <Fonts.ButtonText>Confirmar</Fonts.ButtonText>
            </Button>
          </HStack>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
