export type RecoverPasswordDto = {
  email: string;
  link_to_redirect: string;
};
