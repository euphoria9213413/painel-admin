import {
  Button,
  FormControl,
  HStack,
  Input,
  InputGroup,
  InputRightElement,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  Stack,
  Text,
} from "@chakra-ui/react";
import * as Fonts from "../../../components/texts";
import { CheckIcon } from "../../../components/icons";
import Joi from "joi";
import { GraduateStoreDto } from "../types";
import { useForm } from "react-hook-form";
import { joiResolver } from "@hookform/resolvers/joi";
import InputMask from "react-input-mask";
import { ChangeEvent, useEffect, useMemo, useState } from "react";
import { validateCpf } from "@/utils/validatorUtil";

type Props = {
  isOpen: boolean;
  onClose: () => void;
  onSubmit: (data: GraduateStoreDto) => void;
};

const schema = Joi.object({
  code: Joi.string().required().messages({
    "string.empty": "Código deve ser informado",
  }),
  name: Joi.string().required().messages({
    "string.empty": "Nome deve ser informado",
  }),
  cpf: Joi.string()
    .required()
    .pattern(/[0-9]/)
    .messages({
      "string.empty": "CPF deve ser informado",
      "string.invalid": "CPF inválido",
    })
    .custom((value, helper) => {
      const valueReplaced = value
        .replaceAll(".", "")
        .replaceAll("-", "")
        .replaceAll("_", "");

      const isValidCpf = validateCpf(valueReplaced);

      if (!isValidCpf || valueReplaced.length !== 11) {
        return helper.error("string.invalid");
      }

      return value;
    }),
  class: Joi.string().required().messages({
    "string.empty": "Turma deve ser informada",
  }),
  situation: Joi.number().required(),
});

/* eslint-disable react/no-children-prop */
export const RegisterModal = ({ isOpen, onClose, onSubmit }: Props) => {
  const [cpfError, setCpfError] = useState<boolean | undefined>();

  const {
    register,
    handleSubmit,
    watch,
    setValue,
    clearErrors,
    reset,
    formState: { errors },
  } = useForm<GraduateStoreDto>({
    resolver: joiResolver(schema),
  });
  const cpf = watch("cpf");

  const onChangeCpfValue = (event: ChangeEvent<HTMLInputElement>) => {
    clearErrors("cpf");
    setCpfError(false);

    const value = event.target.value
      .replaceAll(".", "")
      .replaceAll("-", "")
      .replaceAll("_", "");

    const isValidNumberValue = /^[0-9]*$/.test(value);
    const validatedValue = isValidNumberValue ? value : cpf;

    setValue("cpf", validatedValue);
  };

  const onBlurCpfValue = () => {
    if (!cpf) return;

    const value = cpf
      .replaceAll(".", "")
      .replaceAll("-", "")
      .replaceAll("_", "");
    const isValidCpf = validateCpf(value);

    if (!isValidCpf) {
      setCpfError(true);
    }
  };

  const cpfInputColor = useMemo(() => {
    if (!cpf && !cpfError) return "#EAEAEA";
    if (errors?.cpf?.message || cpfError) return "red";

    const cpfWithoutMask = cpf
      .replaceAll(".", "")
      .replaceAll("-", "")
      .replaceAll("_", "");

    if (cpfWithoutMask.length === 11) return "#62CD0F";

    return "#EAEAEA";
  }, [cpf, errors?.cpf?.message, cpfError]);

  useEffect(() => {
    if (!isOpen) {
      reset();
    }
  }, [isOpen]);

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      isCentered
      closeOnEsc={false}
      closeOnOverlayClick={false}
    >
      <ModalOverlay />
      <ModalContent
        left={142}
        bgColor="neutral.100"
        as="form"
        onSubmit={handleSubmit(onSubmit)}
      >
        <ModalHeader>Cadastro de Formando</ModalHeader>
        <ModalBody>
          <Stack spacing={5}>
            <FormControl w="100%">
              <Fonts.FormLabelText>Código do Formando</Fonts.FormLabelText>
              <Input
                type="text"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                {...register("code")}
              />
              {errors?.code?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.code.message}
                </Text>
              )}
            </FormControl>
            <FormControl w="100%">
              <Fonts.FormLabelText>Nome Completo</Fonts.FormLabelText>
              <Input
                type="text"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                {...register("name")}
              />
              {errors?.name?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.name.message}
                </Text>
              )}
            </FormControl>
            <FormControl w="100%">
              <Fonts.FormLabelText>CPF</Fonts.FormLabelText>
              <InputGroup>
                <Input
                  {...register("cpf")}
                  as={InputMask}
                  mask="***.***.***-**"
                  type="text"
                  textIndent={"1px"}
                  w="100%"
                  h={10}
                  borderRadius={3}
                  border="1px solid #EAEAEA"
                  bgColor="neutral.0"
                  borderColor={cpfInputColor}
                  onChange={onChangeCpfValue}
                  onBlur={onBlurCpfValue}
                />
                {cpf &&
                  validateCpf(
                    cpf
                      .replaceAll(".", "")
                      .replaceAll("-", "")
                      .replaceAll("_", "")
                  ) && (
                    <InputRightElement
                      children={<CheckIcon color="green.500" />}
                    />
                  )}
              </InputGroup>
              {errors?.cpf?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.cpf.message}
                </Text>
              )}
            </FormControl>
            <FormControl w="100%">
              <Fonts.FormLabelText>Turma</Fonts.FormLabelText>
              <Input
                type="text"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                {...register("class")}
              />
              {errors?.class?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.class.message}
                </Text>
              )}
            </FormControl>
            <FormControl w="100%">
              <Fonts.FormLabelText>Status</Fonts.FormLabelText>
              <Select
                h={10}
                borderRadius={4}
                bgColor="neutral.0"
                {...register("situation")}
              >
                <option value={1}>Ativo</option>
                <option value={0}>Inativo</option>
              </Select>
            </FormControl>
          </Stack>
        </ModalBody>
        <ModalFooter>
          <HStack w="100%" spacing={7}>
            <Button
              variant="outline"
              borderColor="secondary.500"
              colorScheme="secondary.500"
              w="100%"
              h={9}
              borderRadius={4}
              onClick={onClose}
              _hover={{
                bgColor: "secondary.100",
              }}
              _active={{
                bgColor: "secondary.300",
              }}
            >
              <Fonts.ButtonText color="secondary.500">
                Cancelar
              </Fonts.ButtonText>
            </Button>
            <Button
              variant="solid"
              type="submit"
              bgColor="secondary.500"
              w="100%"
              h={9}
              borderRadius={4}
              _hover={{
                bgColor: "secondary.300",
              }}
              _active={{
                bgColor: "secondary.500",
              }}
            >
              <Fonts.ButtonText>Confirmar</Fonts.ButtonText>
            </Button>
          </HStack>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
