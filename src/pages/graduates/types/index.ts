import { StoreIndexDto } from "@/pages/stores/types";

export type GraduateIndexQueryDto = {
  storeId: number;
  codeOrNameOrClass?: string;
  situation?: string;
  page: number;
};

export type GraduateIndexDto = {
  id: number;
  code: string;
  name: string;
  cpf: string;
  class: string;
  situation: boolean;
  store: StoreIndexDto;
};

export type GraduateIndexPaginatedDto = {
  data: GraduateIndexDto[];
  page: number;
  perPage: number;
  lastPage: number;
  total: number;
};

export type GraduateStoreDto = {
  code: string;
  name: string;
  cpf: string;
  class: string;
  situation: boolean;
};

export type GraduateIndexFilterDto = {
  codeOrNameOrClass: string;
  situation: string;
};

export type GraduateByCodeDto = Omit<GraduateIndexDto, "store">;
