import {
  HStack,
  InputGroup,
  InputLeftElement,
  Select,
  Stack,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Tfoot,
  Th,
  Thead,
  Tr,
  useDisclosure,
} from "@chakra-ui/react";
import { SearchIcon } from "../../components/icons";
import { MainContainer } from "../../components/mainContainer";
import { FilterBar } from "../../components/FilterBar";
import * as Font from "../../components/texts";
import { MenuActionButton } from "../../components/menuActionButton";
import { useCallback, useEffect, useState } from "react";
import { MenuPopUp } from "../../components/menuPopUp";
import { Pagination } from "../../components/pagination";
import * as T from "../../components/table";
import * as Inputs from "../../components/inputs";
import { RegisterModal } from "./RegisterModal";
import { useGraduate } from "@/hooks/useGraduate";
import {
  GraduateIndexDto,
  GraduateIndexFilterDto,
  GraduateStoreDto,
} from "./types";
import LoadingOverlay from "@/components/overlay";
import { maskCPF } from "@/utils/masks";
import { useParams } from "react-router-dom";
import { SubmitHandler } from "react-hook-form";
import { useStore } from "@/hooks/useStore";
import { useCustomToast } from "@/hooks/useToast";

const initialFilter: GraduateIndexFilterDto = {
  codeOrNameOrClass: "",
  situation: "todos",
};

/* eslint-disable react/no-children-prop */
export const Graduates = () => {
  const { storeId } = useParams();
  const [showMenuPopUp, setShowMenuPopUp] = useState<undefined | number>();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { customToast } = useCustomToast();
  const { getStoreName } = useStore();
  const {
    getGraduates,
    graduateStore,
    graduateSituationUpdate,
    graduateImport,
  } = useGraduate();
  const [page, setPage] = useState<number>(1);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [storeName, setStoreName] = useState<string | undefined>();
  const [graduates, setGraduates] = useState<GraduateIndexDto[]>([]);
  const [totalGraduates, setTotalGraduates] = useState<number>(0);
  const [lastPage, setLastPage] = useState<number>(1);
  const [filter, setFilter] = useState<GraduateIndexFilterDto>(initialFilter);
  const [stopedTyping, setStopedTyping] = useState(false);
  const [typingTimer, setTypingTimer] = useState<NodeJS.Timeout | undefined>();

  useEffect(() => {
    getStoreName(Number(storeId))
      .then((response) => setStoreName(response.data.name))
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      );
  }, []);

  const handleInputChange = () => {
    clearTimeout(typingTimer);

    const newTypingTimer = setTimeout(() => {
      setStopedTyping(true);
    }, 500);

    setTypingTimer(newTypingTimer);
  };

  const filters = [
    {
      field: (
        <InputGroup alignItems="center" key={1}>
          <InputLeftElement
            pointerEvents="none"
            children={<SearchIcon />}
            top=""
            left={1}
          />
          <Inputs.SimpleText
            placeholder="Buscar por ID nome ou centro de controle"
            w="100%"
            h={9}
            borderRadius={4}
            textIndent="35px"
            bg="neutral.0"
            onChange={(event) => {
              setFilter({ ...filter, codeOrNameOrClass: event.target.value });
              handleInputChange();
            }}
          />
        </InputGroup>
      ),
    },
    {
      field: (
        <HStack w={260} justifyContent="flex-end" key={2}>
          <Text
            w={100}
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="400"
            fontSize="14px"
            lineHeight="17px"
            color="neutral.500"
          >
            Filtrar por status:
          </Text>
          <Select
            width={110}
            h={9}
            borderRadius={4}
            bg="neutral.0"
            onChange={(event) =>
              setFilter({ ...filter, situation: event.target.value })
            }
          >
            <option value="">Todos</option>
            <option value="ativos">Ativos</option>
            <option value="inativos">Inativos</option>
          </Select>
        </HStack>
      ),
    },
  ];

  const callbackGraduates = useCallback(() => {
    setIsLoading(true);

    getGraduates({ page, storeId: Number(storeId), ...filter })
      .then((response) => {
        setGraduates(response.data.data);
        setTotalGraduates(response.data.total);
        setLastPage(response.data.lastPage);
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  }, [page, filter]);

  useEffect(() => callbackGraduates(), [page, filter.situation]);
  useEffect(() => {
    if (stopedTyping) {
      if (page === 1) {
        callbackGraduates();
      } else {
        setPage(1);
      }
      setStopedTyping(false);
    }
  }, [filter.codeOrNameOrClass, stopedTyping]);

  const onSubmit: SubmitHandler<GraduateStoreDto> = (props) => {
    const data: GraduateStoreDto = {
      ...props,
      situation: props.situation ? true : false,
    };
    setIsLoading(true);

    graduateStore(Number(storeId), data)
      .then(() => {
        customToast("success", "Cadastrado com sucesso");
        callbackGraduates();
        onClose();
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  };

  const onImportFile = (file: any) => {
    setIsLoading(true);

    graduateImport(Number(storeId), file)
      .then(() => {
        customToast("success", "Importado com sucesso");
        callbackGraduates();
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  };

  return (
    <MainContainer
      height={isLoading || graduates.length < 10 ? undefined : "100%"}
      title={`Minhas Lojas > ${storeName} > Formandos`}
    >
      <FilterBar
        title={`Formandos cadastrados (${totalGraduates})`}
        subTitle="Lista de formandos vendedores"
        buttons={[
          {
            title: "Adicionar formando",
            onClick: onOpen,
          },
          {
            title: "Importar planilha",
            onClick: onImportFile,
            type: "inputFile",
          },
        ]}
        filters={filters}
      />
      {isLoading && <LoadingOverlay />}
      <Stack w="100%" p={5} mt="0 !important">
        <TableContainer>
          <Table w="100%">
            <Thead>
              <Tr>
                <Th textAlign="center">
                  <Font.TableTheadText>Código</Font.TableTheadText>
                  <Font.TableTheadText>Formando</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Nome</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>CPF</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Turma</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Status</Font.TableTheadText>
                </Th>
                <Th></Th>
              </Tr>
            </Thead>
            <Tbody>
              {graduates.map((graduate) => (
                <T.Body key={graduate.id}>
                  <Td textAlign="center">
                    <Font.TableBodyText>{graduate.code}</Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>{graduate.name}</Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>
                      {maskCPF(graduate.cpf)}
                    </Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>{graduate.class}</Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>
                      {graduate.situation ? "Ativo" : "Inativo"}
                    </Font.TableBodyText>
                  </Td>
                  <Td>
                    <MenuActionButton
                      color="#2F2F2F"
                      onClick={() => setShowMenuPopUp(graduate.id)}
                      _hover={{
                        bgColor: "neutral.200",
                      }}
                      _active={{
                        bgColor: "neutral.300",
                      }}
                    />
                  </Td>
                  {showMenuPopUp === graduate.id && (
                    <MenuPopUp
                      top="-4px"
                      right="36px"
                      options={[
                        {
                          title: graduate.situation ? "Inativar" : "Ativar",
                          onClick: () => {
                            setIsLoading(true);
                            graduateSituationUpdate(
                              graduate.id,
                              !graduate.situation
                            ).finally(() => callbackGraduates());
                          },
                        },
                      ]}
                      setShowMenuPopUp={setShowMenuPopUp}
                    />
                  )}
                </T.Body>
              ))}
            </Tbody>
            <Tfoot w="100%" h="15px"></Tfoot>
          </Table>
          <Pagination page={page} lastPage={lastPage} onClick={setPage} />
        </TableContainer>
        <RegisterModal isOpen={isOpen} onClose={onClose} onSubmit={onSubmit} />
      </Stack>
    </MainContainer>
  );
};
