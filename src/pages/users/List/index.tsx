import {
  HStack,
  InputGroup,
  InputLeftElement,
  Select,
  Stack,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Tfoot,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import { MainContainer } from "../../../components/mainContainer";
import { FilterBar } from "../../../components/FilterBar";
import { SearchIcon } from "../../../components/icons";
import { Pagination } from "../../../components/pagination";
import * as Fonts from "../../../components/texts";
import * as T from "../../../components/table";
import { MenuActionButton } from "../../../components/menuActionButton";
import { MenuPopUp } from "../../../components/menuPopUp";
import { useCallback, useEffect, useState } from "react";
import * as Inputs from "../../../components/inputs";
import { useNavigate } from "react-router-dom";
import { UserIndexDto, UserIndexFilterDto } from "../types";
import LoadingOverlay from "@/components/overlay";
import { ProfileTypeEnum } from "../enums";
import { useUser } from "@/hooks/useUser";
import { useCustomToast } from "@/hooks/useToast";

const initialFilter: UserIndexFilterDto = {
  nameOrEmail: "",
  profileType: "",
  situation: "todos",
};

/* eslint-disable react/no-children-prop */
export const Users = () => {
  const navigate = useNavigate();
  const [showMenuPopUp, setShowMenuPopUp] = useState<undefined | number>();
  const { getUsers, userSituationUpdate } = useUser();
  const { customToast } = useCustomToast();
  const [page, setPage] = useState<number>(1);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [users, setUsers] = useState<UserIndexDto[]>([]);
  const [totalUSers, setTotalUSers] = useState<number>(0);
  const [lastPage, setLastPage] = useState<number>(1);
  const [filter, setFilter] = useState<UserIndexFilterDto>(initialFilter);
  const [stopedTyping, setStopedTyping] = useState(false);
  const [typingTimer, setTypingTimer] = useState<NodeJS.Timeout | undefined>();

  const handleInputChange = () => {
    clearTimeout(typingTimer);

    const newTypingTimer = setTimeout(() => {
      setStopedTyping(true);
    }, 500);

    setTypingTimer(newTypingTimer);
  };

  const filters = [
    {
      field: (
        <InputGroup alignItems="center" key={1}>
          <InputLeftElement
            pointerEvents="none"
            children={<SearchIcon />}
            top=""
            left={1}
          />
          <Inputs.SimpleText
            placeholder="Buscar por nome ou e-mail"
            w="100%"
            h={9}
            borderRadius={4}
            textIndent="35px"
            bg="neutral.0"
            onChange={(event) => {
              setFilter({ ...filter, nameOrEmail: event.target.value });
              handleInputChange();
            }}
          />
        </InputGroup>
      ),
    },
    {
      field: (
        <HStack w={260} justifyContent="flex-end" key={2}>
          <Text
            w={100}
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="400"
            fontSize="14px"
            lineHeight="17px"
            color="neutral.500"
          >
            Filtrar por tipo:
          </Text>
          <Select
            width={110}
            h={9}
            borderRadius={4}
            bg="neutral.0"
            onChange={(event) =>
              setFilter({ ...filter, profileType: event.target.value })
            }
          >
            <option value="">Todos</option>
            <option value={ProfileTypeEnum.administrador}>Administrador</option>
            <option value={ProfileTypeEnum.cliente}>Cliente</option>
          </Select>
        </HStack>
      ),
    },
    {
      field: (
        <HStack w={260} justifyContent="flex-end" key={3}>
          <Text
            w={100}
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="400"
            fontSize="14px"
            lineHeight="17px"
            color="neutral.500"
          >
            Filtrar por status:
          </Text>
          <Select
            width={110}
            h={9}
            borderRadius={4}
            bg="neutral.0"
            onChange={(event) =>
              setFilter({ ...filter, situation: event.target.value })
            }
          >
            <option value="">Todos</option>
            <option value="ativos">Ativos</option>
            <option value="inativos">Inativos</option>
          </Select>
        </HStack>
      ),
    },
  ];

  const callbackUsers = useCallback(() => {
    setIsLoading(true);

    getUsers({ page, ...filter })
      .then((response) => {
        setUsers(response.data.data);
        setTotalUSers(response.data.total);
        setLastPage(response.data.lastPage);
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  }, [page, filter]);

  useEffect(
    () => callbackUsers(),
    [page, filter.situation, filter.profileType]
  );
  useEffect(() => {
    if (stopedTyping) {
      if (page === 1) {
        callbackUsers();
      } else {
        setPage(1);
      }
      setStopedTyping(false);
    }
  }, [filter.nameOrEmail, stopedTyping]);

  return (
    <MainContainer
      height={isLoading || users.length < 10 ? undefined : "100%"}
      title="Configurações > Usuários"
    >
      <FilterBar
        title={`Usuários cadastrados (${totalUSers})`}
        buttons={[
          {
            title: "Adicionar um usuário",
            onClick: () => navigate("/usuario/cadastro"),
          },
        ]}
        filters={filters}
      />
      {isLoading && <LoadingOverlay />}
      <Stack w="100%" p={5} mt="0 !important">
        <TableContainer>
          <Table w="100%">
            <Thead>
              <Tr>
                <Th textAlign="center">
                  <Fonts.TableTheadText>Nome</Fonts.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Fonts.TableTheadText>E-mail</Fonts.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Fonts.TableTheadText>Perfil</Fonts.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Fonts.TableTheadText>Status</Fonts.TableTheadText>
                </Th>
                <Th></Th>
              </Tr>
            </Thead>
            <Tbody>
              {users.map((user) => (
                <T.Body key={user.id}>
                  <Td textAlign="center">
                    <Fonts.TableBodyText>{user.name}</Fonts.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Fonts.TableBodyText>{user.email}</Fonts.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Fonts.TableBodyText>
                      {user.profile.description}
                    </Fonts.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Fonts.TableBodyText>
                      {user.situation ? "Ativo" : "Inativo"}
                    </Fonts.TableBodyText>
                  </Td>
                  <Td>
                    <MenuActionButton
                      color="#2F2F2F"
                      onClick={() => setShowMenuPopUp(user.id)}
                      _hover={{
                        bgColor: "neutral.200",
                      }}
                      _active={{
                        bgColor: "neutral.300",
                      }}
                    />
                  </Td>
                  {showMenuPopUp === user.id && (
                    <MenuPopUp
                      top="-4px"
                      right="35px"
                      options={[
                        {
                          title: "Editar",
                          onClick: () => navigate(`/usuario/editar/${user.id}`),
                        },
                        {
                          title: user.situation ? "Inativar" : "Ativar",
                          onClick: () => {
                            setIsLoading(true);
                            userSituationUpdate(
                              user.id,
                              !user.situation
                            ).finally(() => callbackUsers());
                          },
                        },
                      ]}
                      setShowMenuPopUp={setShowMenuPopUp}
                    />
                  )}
                </T.Body>
              ))}
            </Tbody>
            <Tfoot w="100%" h="15px"></Tfoot>
          </Table>
          <Pagination page={page} lastPage={lastPage} onClick={setPage} />
        </TableContainer>
      </Stack>
    </MainContainer>
  );
};
