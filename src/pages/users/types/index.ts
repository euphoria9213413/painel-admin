import { ProfileTypeType } from "../enums";

export type UserIndexQueryDto = {
  nameOrEmail?: string;
  profileType?: string;
  situation?: string;
  page: number;
};

export type UserIndexFilterDto = Omit<UserIndexQueryDto, "page">;

export type ProfileDataDto = {
  id: number;
  type: ProfileTypeType;
  description: string;
};

export type UserIndexDto = {
  id: number;
  name: string;
  email: string;
  profile: ProfileDataDto;
  situation: boolean;
};

export type UserIndexPaginatedDto = {
  data: UserIndexDto[];
  page: number;
  perPage: number;
  lastPage: number;
  total: number;
};

export type UserStoreDto = {
  name: string;
  email: string;
  cpf: string;
  birth?: string;
  contact?: string;
  situation: boolean | number;
  profileId: number;
};

export type UserPasswordUpdateDto = {
  currentPassword: string;
  newPassword: string;
  newPasswordConfirmation: string;
};

export type UserEmailUpdateDto = {
  currentEmail: string;
  newEmail: string;
  password: string;
};

export type UserDataDto = {
  id?: number;
  name: string;
  email: string;
  cpf: string;
  birth: string;
  contact: string;
  situation: boolean;
  profile?: ProfileDataDto;
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
};
