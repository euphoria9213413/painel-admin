import {
  Button,
  FormControl,
  HStack,
  Input,
  InputGroup,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  Text,
} from "@chakra-ui/react";
import * as Fonts from "../../../../components/texts";
import { SubmitHandler, useForm } from "react-hook-form";
import { UserPasswordUpdateDto } from "../../types";
import { joiResolver } from "@hookform/resolvers/joi";
import Joi from "joi";
import { useUser } from "@/hooks/useUser";
import { useCustomToast } from "@/hooks/useToast";
import { useLogin } from "@/hooks/useLogin";

type Props = {
  id: number;
  isOpen: boolean;
  onClose: () => void;
  setIsLoading: (value: boolean) => void;
};

const passwordSchema = Joi.object({
  currentPassword: Joi.string().required().messages({
    "string.empty": "Senha atual deve ser informada",
  }),
  newPassword: Joi.string()
    .min(6)
    .required()
    .invalid(Joi.ref("currentPassword"))
    .messages({
      "string.empty": "Nova senha deve ser informada",
      "string.min": "Sonva senha deve ter no mínimo 6 caracteres",
      "any.invalid": "Nova senha deve ser diferente da senha atual",
    }),
  newPasswordConfirmation: Joi.string()
    .required()
    .equal(Joi.ref("newPassword"))
    .messages({
      "string.empty": "Confirmação de senha deve ser informada",
      "any.only": "Confirmação de senha não confere com a senha",
    }),
});

/* eslint-disable react/no-children-prop */
export const PasswordFormModal = ({
  id,
  isOpen,
  onClose,
  setIsLoading,
}: Props) => {
  const { userPasswordUpdate } = useUser();
  const { setAuthentication } = useLogin();
  const { customToast } = useCustomToast();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<UserPasswordUpdateDto>({
    resolver: joiResolver(passwordSchema),
  });

  const onSubmit: SubmitHandler<UserPasswordUpdateDto> = (props) => {
    setIsLoading(true);

    userPasswordUpdate(id, props)
      .then((response) => {
        setAuthentication(response.data);
        customToast("success", "Senha alterada com sucesso");
        onClose();
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  };

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      isCentered
      closeOnEsc={false}
      closeOnOverlayClick={false}
    >
      <ModalOverlay />
      <ModalContent
        left={142}
        bgColor="neutral.100"
        as="form"
        onSubmit={handleSubmit(onSubmit)}
      >
        <ModalHeader>Alterar senha</ModalHeader>
        <ModalBody>
          <Stack spacing={5}>
            <FormControl w="100%">
              <Fonts.FormLabelText>Senha atual*</Fonts.FormLabelText>
              <Input
                type="password"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                {...register("currentPassword")}
              />
              {errors?.currentPassword?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.currentPassword.message}
                </Text>
              )}
            </FormControl>
            <FormControl w="100%">
              <Fonts.FormLabelText>Nova senha*</Fonts.FormLabelText>
              <InputGroup>
                <Input
                  type="password"
                  textIndent={"1px"}
                  w="100%"
                  h={10}
                  borderRadius={3}
                  border="1px solid #EAEAEA"
                  bgColor="neutral.0"
                  {...register("newPassword")}
                />
              </InputGroup>
              {errors?.newPassword?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.newPassword.message}
                </Text>
              )}
            </FormControl>
            <FormControl w="100%">
              <Fonts.FormLabelText>
                Confirmação de nova senha*
              </Fonts.FormLabelText>
              <Input
                type="password"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                {...register("newPasswordConfirmation")}
              />
              {errors?.newPasswordConfirmation?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.newPasswordConfirmation.message}
                </Text>
              )}
            </FormControl>
          </Stack>
        </ModalBody>
        <ModalFooter>
          <HStack w="100%" spacing={7}>
            <Button
              variant="outline"
              borderColor="secondary.500"
              colorScheme="secondary.500"
              w="100%"
              h={9}
              borderRadius={4}
              onClick={onClose}
              _hover={{
                bgColor: "secondary.100",
              }}
              _active={{
                bgColor: "secondary.300",
              }}
            >
              <Fonts.ButtonText color="secondary.500">
                Cancelar
              </Fonts.ButtonText>
            </Button>
            <Button
              variant="solid"
              bgColor="secondary.500"
              w="100%"
              h={9}
              borderRadius={4}
              type="submit"
              _hover={{
                bgColor: "secondary.300",
              }}
              _active={{
                bgColor: "secondary.500",
              }}
            >
              <Fonts.ButtonText>Confirmar</Fonts.ButtonText>
            </Button>
          </HStack>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
