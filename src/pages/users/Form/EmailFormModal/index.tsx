import {
  Button,
  FormControl,
  HStack,
  Input,
  InputGroup,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  Text,
} from "@chakra-ui/react";
import * as Fonts from "../../../../components/texts";
import { SubmitHandler, useForm } from "react-hook-form";
import { UserEmailUpdateDto } from "../../types";
import { joiResolver } from "@hookform/resolvers/joi";
import Joi from "joi";
import { useUser } from "@/hooks/useUser";
import { useCustomToast } from "@/hooks/useToast";

type Props = {
  id: number;
  isOpen: boolean;
  onClose: () => void;
  setIsLoading: (value: boolean) => void;
};

const passwordSchema = Joi.object({
  currentEmail: Joi.string().required().messages({
    "string.empty": "E-mail atual deve ser informado",
    "any.invalid": "E-mail atual deve ser informado",
  }),
  newEmail: Joi.string().required().messages({
    "string.empty": "Novo e-mail deve ser informado",
  }),
  password: Joi.string().required().messages({
    "string.empty": "Senha deve ser informada",
  }),
});

const initialValues = {
  currentEmail: "",
  newEmail: "",
  password: "",
};

/* eslint-disable react/no-children-prop */
export const EmailFormModal = ({
  id,
  isOpen,
  onClose,
  setIsLoading,
}: Props) => {
  const { userEmailUpdate } = useUser();
  const { customToast } = useCustomToast();

  const {
    register,
    handleSubmit,
    setValue,
    clearErrors,
    watch,
    formState: { errors },
  } = useForm<UserEmailUpdateDto>({
    defaultValues: initialValues,
    resolver: joiResolver(passwordSchema),
  });

  const currentEmail = watch("currentEmail");
  const newEmail = watch("newEmail");

  const onSubmit: SubmitHandler<UserEmailUpdateDto> = (props) => {
    setIsLoading(true);

    userEmailUpdate(id, props)
      .then(() => {
        customToast("success", "E-mail alterado com sucesso");
        onClose();
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  };

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      isCentered
      closeOnEsc={false}
      closeOnOverlayClick={false}
    >
      <ModalOverlay />
      <ModalContent
        left={142}
        bgColor="neutral.100"
        as="form"
        onSubmit={handleSubmit(onSubmit)}
      >
        <ModalHeader>Alterar e-mail</ModalHeader>
        <ModalBody>
          <Stack spacing={5}>
            <FormControl w="100%">
              <Fonts.FormLabelText>E-mail atual*</Fonts.FormLabelText>
              <Input
                type="email"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                value={currentEmail}
                onChange={(event) => {
                  clearErrors("currentEmail");
                  setValue("currentEmail", event.target.value.toLowerCase());
                }}
              />
              {errors?.currentEmail?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.currentEmail.message}
                </Text>
              )}
            </FormControl>
            <FormControl w="100%">
              <Fonts.FormLabelText>Novo e-mail*</Fonts.FormLabelText>
              <InputGroup>
                <Input
                  type="email"
                  textIndent={"1px"}
                  w="100%"
                  h={10}
                  borderRadius={3}
                  border="1px solid #EAEAEA"
                  bgColor="neutral.0"
                  value={newEmail}
                  onChange={(event) => {
                    clearErrors("newEmail");
                    setValue("newEmail", event.target.value.toLowerCase());
                  }}
                />
              </InputGroup>
              {errors?.newEmail?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.newEmail.message}
                </Text>
              )}
            </FormControl>
            <FormControl w="100%">
              <Fonts.FormLabelText>Senha*</Fonts.FormLabelText>
              <Input
                type="password"
                textIndent={"1px"}
                w="100%"
                h={10}
                borderRadius={3}
                border="1px solid #EAEAEA"
                bgColor="neutral.0"
                {...register("password")}
              />
              {errors?.password?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.password.message}
                </Text>
              )}
            </FormControl>
          </Stack>
        </ModalBody>
        <ModalFooter>
          <HStack w="100%" spacing={7}>
            <Button
              variant="outline"
              borderColor="secondary.500"
              colorScheme="secondary.500"
              w="100%"
              h={9}
              borderRadius={4}
              onClick={onClose}
              _hover={{
                bgColor: "secondary.100",
              }}
              _active={{
                bgColor: "secondary.300",
              }}
            >
              <Fonts.ButtonText color="secondary.500">
                Cancelar
              </Fonts.ButtonText>
            </Button>
            <Button
              variant="solid"
              bgColor="secondary.500"
              w="100%"
              h={9}
              borderRadius={4}
              type="submit"
              _hover={{
                bgColor: "secondary.300",
              }}
              _active={{
                bgColor: "secondary.500",
              }}
            >
              <Fonts.ButtonText>Confirmar</Fonts.ButtonText>
            </Button>
          </HStack>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
