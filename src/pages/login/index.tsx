import {
  Button,
  Checkbox,
  FormControl,
  HStack,
  Input,
  Text,
  VStack,
} from "@chakra-ui/react";
import { AccountBox } from "../../components/accountBox";
import { HomeBackground } from "../../components/homeBackground";
import { useNavigate } from "react-router-dom";
import { SubmitHandler, useForm } from "react-hook-form";
import { LoginSubmitDto } from "./types/login";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";
import { useLogin } from "@/hooks/useLogin";
import { useState } from "react";
import { useCustomToast } from "@/hooks/useToast";

const schema = Joi.object({
  email: Joi.string().required().messages({
    "string.empty": "E-mail deve ser informado",
  }),
  password: Joi.string().required().messages({
    "string.empty": "Senha deve ser informada",
  }),
  rememberMe: Joi.boolean().allow(null),
});

export const Login = () => {
  const { doLogin, setAuthentication } = useLogin();
  const navigate = useNavigate();
  const { customToast } = useCustomToast();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginSubmitDto>({
    resolver: joiResolver(schema),
  });

  const onSubmit: SubmitHandler<LoginSubmitDto> = (props) => {
    setIsLoading(true);

    doLogin(props)
      .then((response) => {
        setAuthentication(response.data);
        navigate("/");
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  };

  return (
    <HomeBackground>
      <AccountBox>
        <VStack
          w="100%"
          spacing={7}
          as="form"
          onSubmit={handleSubmit(onSubmit)}
        >
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="400"
            fontSize="30px"
            lineHeight="36px"
            color="neutral.400"
          >
            Entrar
          </Text>
          <Text
            fontFamily="Lato"
            fontStyle="normal"
            fontWeight="500"
            fontSize="15px"
            lineHeight="18px"
            color="neutral.50"
          >
            Insira seus dados
          </Text>

          <VStack w="100%" spacing={7}>
            <FormControl w="100%">
              <Input
                type="email"
                placeholder="E-mail"
                _placeholder={{ color: "neutral.50" }}
                w="100%"
                h={50}
                borderRadius="50px"
                textIndent="15px"
                border={`1px solid ${
                  errors?.email?.message ? "red" : "#EAEAEA"
                }`}
                bgColor="neutral.0"
                {...register("email")}
              />
              {errors?.email?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.email.message}
                </Text>
              )}
            </FormControl>
            <FormControl w="100%">
              <Input
                type="password"
                placeholder="Senha"
                _placeholder={{ color: "neutral.50" }}
                w="100%"
                h={50}
                borderRadius="50px"
                textIndent="15px"
                border={`1px solid ${
                  errors?.password?.message ? "red" : "#EAEAEA"
                }`}
                bgColor="neutral.0"
                {...register("password")}
              />
              {errors?.password?.message && (
                <Text
                  fontFamily="Lato"
                  fontStyle="normal"
                  fontWeight="500"
                  fontSize="11px"
                  lineHeight="18px"
                  color="fail.500"
                >
                  {errors.password.message}
                </Text>
              )}
            </FormControl>
          </VStack>
          <HStack w="100%" justifyContent="space-between">
            <Checkbox
              size="lg"
              fontFamily="Lato"
              fontWeight="400"
              fontSize="15px"
              lineHeight="18px"
              colorScheme="orange"
              borderColor="secondary.500"
              {...register("rememberMe")}
            >
              <Text
                fontFamily="Lato"
                fontStyle="normal"
                fontWeight="400"
                fontSize="15px"
                lineHeight="18px"
                color="neutral.400"
                cursor="pointer"
              >
                Lembre-me
              </Text>
            </Checkbox>
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="400"
              fontSize="15px"
              lineHeight="18px"
              color="secondary.500"
              cursor="pointer"
              onClick={() => navigate("/recuperar-senha")}
            >
              Recuperar senha
            </Text>
          </HStack>

          <Button
            w="100%"
            borderRadius="50px"
            backgroundColor="neutral.700"
            textColor="neutral.0"
            h="50px"
            colorScheme="neutral.700"
            variant="solid"
            type="submit"
            isLoading={isLoading}
            _hover={{
              bgColor: "neutral.400",
            }}
            _active={{
              bgColor: "neutral.700",
            }}
          >
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="400"
              fontSize="16px"
              lineHeight="24px"
              cursor="pointer"
            >
              entrar
            </Text>
          </Button>

          <HStack w="500px" justifyContent="center">
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="400"
              fontSize="15px"
              lineHeight="18px"
              color="neutral.400"
            >
              Não tem uma conta?
            </Text>
            <Text
              fontFamily="Lato"
              fontStyle="normal"
              fontWeight="400"
              fontSize="15px"
              lineHeight="18px"
              color="secondary.500"
              cursor="pointer"
              onClick={() => navigate("/criar-conta")}
            >
              Cadastre-se
            </Text>
          </HStack>
        </VStack>
      </AccountBox>
    </HomeBackground>
  );
};
