export type LoginSubmitDto = {
  email: string;
  password: string;
  rememberMe: boolean;
};

export type LoginAuthenticated = {
  accessToken: string;
  id: number;
  name: string;
  profile: string;
};

export type StoreAccountDto = {
  name: string;
  email: string;
  cpf: string;
  birth: string;
  password: string;
  passwordConfirmation: string;
};
