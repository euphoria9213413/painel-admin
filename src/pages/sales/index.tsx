import {
  InputGroup,
  InputLeftElement,
  Stack,
  Table,
  TableContainer,
  Tbody,
  Td,
  Tfoot,
  Th,
  Thead,
  Tr,
  useDisclosure,
} from "@chakra-ui/react";
import { MainContainer } from "../../components/mainContainer";
import { FilterBar } from "../../components/FilterBar";
import * as Inputs from "../../components/inputs";
import * as Font from "../../components/texts";
import { Pagination } from "../../components/pagination";
import * as T from "../../components/table";
import { SearchIcon } from "@/components/icons";
import { useParams } from "react-router-dom";
import { useSale } from "@/hooks/useSale";
import { useCallback, useEffect, useState } from "react";
import {
  SaleEditDto,
  SaleIndexDto,
  SaleIndexFilterDto,
  SaleStatusUpdateDto,
} from "./types";
import LoadingOverlay from "@/components/overlay";
import { useCustomToast } from "@/hooks/useToast";
import { moneyFormat } from "@/utils/formatUtil";
import { PaymentStatusEnum, PaymentTypeEnum } from "./enums";
import { MenuActionButton } from "@/components/menuActionButton";
import { MenuPopUp } from "@/components/menuPopUp";
import { StatusModal } from "./statusModal";
import { EditFormModal } from "./editFormModal";
import { PixModal } from "./pixModal";

const initialFilter: SaleIndexFilterDto = {
  graduateName: "",
  buyerName: "",
  accountName: "",
};

/* eslint-disable react/no-children-prop */
export const Sales = () => {
  const { storeId } = useParams();
  const { customToast } = useCustomToast();
  const { getSales, exportData, statusUpdate, edit } = useSale();
  const [page, setPage] = useState<number>(1);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [storeName, setStoreName] = useState<string | undefined>();
  const [sales, setSales] = useState<SaleIndexDto[]>([]);
  const [totalSales, setTotalSales] = useState<number>(0);
  const [lastPage, setLastPage] = useState<number>(1);
  const [filter, setFilter] = useState<SaleIndexFilterDto>(initialFilter);
  const [stopedTyping, setStopedTyping] = useState(false);
  const [typingTimer, setTypingTimer] = useState<NodeJS.Timeout | undefined>();
  const [showMenuPopUp, setShowMenuPopUp] = useState<undefined | number>();
  const {
    isOpen: statusIsOpen,
    onOpen: statusOnOpen,
    onClose: statusOnClose,
  } = useDisclosure();
  const {
    isOpen: pixIsOpen,
    onOpen: pixOnOpen,
    onClose: pixOnClose,
  } = useDisclosure();
  const [isOpenEditForm, setIsOpenEditForm] = useState<boolean>(false);
  const [saleToEdit, setSaleToEdit] = useState<SaleIndexDto>();
  const [selectedSale, setSelectedSale] = useState<SaleIndexDto>();

  useEffect(() => {
    if (showMenuPopUp) {
      setSaleToEdit(sales.find((value) => value.id === showMenuPopUp));
    }
  }, [showMenuPopUp]);

  const handleInputChange = () => {
    clearTimeout(typingTimer);

    const newTypingTimer = setTimeout(() => {
      setStopedTyping(true);
    }, 500);

    setTypingTimer(newTypingTimer);
  };

  const filters = [
    {
      field: (
        <InputGroup alignItems="center" key={1}>
          <InputLeftElement
            pointerEvents="none"
            children={<SearchIcon />}
            top=""
            left={1}
          />
          <Inputs.SimpleText
            placeholder="Buscar por dados do usuário"
            w="100%"
            h={9}
            borderRadius={4}
            textIndent="35px"
            bg="neutral.0"
            onChange={(event) => {
              setFilter({ ...filter, accountName: event.target.value });
              handleInputChange();
            }}
          />
        </InputGroup>
      ),
    },
    {
      field: (
        <InputGroup alignItems="center" key={2}>
          <InputLeftElement
            pointerEvents="none"
            children={<SearchIcon />}
            top=""
            left={1}
          />
          <Inputs.SimpleText
            placeholder="Buscar por dados do formando"
            w="100%"
            h={9}
            borderRadius={4}
            textIndent="35px"
            bg="neutral.0"
            onChange={(event) => {
              setFilter({ ...filter, graduateName: event.target.value });
              handleInputChange();
            }}
          />
        </InputGroup>
      ),
    },
    {
      field: (
        <InputGroup alignItems="center" key={3}>
          <InputLeftElement
            pointerEvents="none"
            children={<SearchIcon />}
            top=""
            left={1}
          />
          <Inputs.SimpleText
            placeholder="Buscar por dados do comprador"
            w="100%"
            h={9}
            borderRadius={4}
            textIndent="35px"
            bg="neutral.0"
            onChange={(event) => {
              setFilter({ ...filter, buyerName: event.target.value });
              handleInputChange();
            }}
          />
        </InputGroup>
      ),
    },
  ];

  const menuOptions = (sale: SaleIndexDto) => {
    if (
      sale.paymentType === PaymentTypeEnum.pix &&
      PaymentStatusEnum[sale.paymentStatus] === PaymentStatusEnum.ativa
    ) {
      return [
        {
          title: "Alterar status da venda",
          onClick: () => statusOnOpen(),
        },
        {
          title: "Editar venda",
          onClick: () => setIsOpenEditForm(true),
        },
        {
          title: "Copiar código PIX",
          onClick: () => {
            setSelectedSale(sale);
            pixOnOpen();
          },
        },
      ];
    }

    return [
      {
        title: "Alterar status da venda",
        onClick: () => statusOnOpen(),
      },
      {
        title: "Editar venda",
        onClick: () => setIsOpenEditForm(true),
      },
    ];
  };

  const callbackSales = useCallback(() => {
    setIsLoading(true);

    getSales(Number(storeId), { page, ...filter })
      .then((response) => {
        setSales(response.data.data);
        setStoreName(response.data.data?.[0].storeName);
        setTotalSales(response.data.total);
        setLastPage(response.data.lastPage);
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  }, [page, filter]);

  useEffect(() => callbackSales(), [page]);
  useEffect(() => {
    if (stopedTyping) {
      if (page === 1) {
        callbackSales();
      } else {
        setPage(1);
      }
      setStopedTyping(false);
    }
  }, [filter, stopedTyping]);

  const exportSales = useCallback(() => {
    setIsLoading(true);

    exportData(Number(storeId), { page, ...filter })
      .then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", `AEA-Vendas-Loja(${storeName}).xlsx`);
        link.click();

        customToast("success", "Exportado com sucesso");
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  }, [page, filter]);

  const onSubmit = (id: number, data: SaleStatusUpdateDto) => {
    setIsLoading(true);

    statusUpdate(id, data.status)
      .then(() => {
        customToast("success", "Status alterado com sucesso");
        callbackSales();
        statusOnClose();
      })
      .catch((error) =>
        customToast("error", [error.response.data.message].join(", "))
      )
      .finally(() => setIsLoading(false));
  };

  /* eslint-disable @typescript-eslint/no-unused-vars */
  const onEditSubmit = (id: number, data: SaleEditDto) => {
    setIsLoading(true);

    const { graduateCode, ...form } = data;

    const formToSubmit = Object.fromEntries(
      Object.entries(form).filter((value) => value[1])
    );

    if (formToSubmit.buyerContact) {
      formToSubmit.buyerContact = `${formToSubmit.buyerContact}`
        .replaceAll("(", "")
        .replaceAll(")", "")
        .replaceAll("-", "")
        .replaceAll(" ", "");
    }

    if (!Object.entries(formToSubmit).length) {
      setIsLoading(false);
      return;
    }

    edit(id, formToSubmit)
      .then(() => setIsOpenEditForm(false))
      .catch((error) => {
        customToast("error", [error.response.data.message].join(", "));
        setIsLoading(false);
      })
      .finally(() => callbackSales());
  };

  return (
    <MainContainer
      height={isLoading || sales.length < 10 ? undefined : "100%"}
      title={`Minhas Lojas > ${storeName} > Vendas`}
    >
      <FilterBar
        title={`Vendas (${totalSales})`}
        subTitle="Lista de vendas realizadas"
        buttons={[
          {
            title: "Exportar dados",
            onClick: exportSales,
          },
        ]}
        filters={filters}
      />
      {isLoading && <LoadingOverlay />}
      <Stack w="100%" p={5} mt="0 !important">
        <TableContainer>
          <Table w="100%">
            <Thead>
              <Tr>
                <Th textAlign="center">
                  <Font.TableTheadText>Número da</Font.TableTheadText>
                  <Font.TableTheadText>Sorte</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Código</Font.TableTheadText>
                  <Font.TableTheadText>Formando</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Nome</Font.TableTheadText>
                  <Font.TableTheadText>Formando</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Nome</Font.TableTheadText>
                  <Font.TableTheadText>Comprador</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Telefone</Font.TableTheadText>
                  <Font.TableTheadText>Comprador</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Data</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Hora</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Produto</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Valor</Font.TableTheadText>
                </Th>
                <Th textAlign="center">
                  <Font.TableTheadText>Status</Font.TableTheadText>
                </Th>
                <Th></Th>
              </Tr>
            </Thead>
            <Tbody>
              {sales.map((sale) => (
                <T.Body key={sale.id}>
                  <Td textAlign="center">
                    <Font.TableBodyText>{sale.luckyNumber}</Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>{sale.graduateCode}</Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>{sale.graduateName}</Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>{sale.buyerName}</Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>{sale.buyerContact}</Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>{sale.date}</Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>{sale.hour}</Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>{sale.productName}</Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>
                      {moneyFormat(Number(sale.price))}
                    </Font.TableBodyText>
                  </Td>
                  <Td textAlign="center">
                    <Font.TableBodyText>
                      {PaymentStatusEnum[sale.paymentStatus]}
                    </Font.TableBodyText>
                  </Td>
                  <Td>
                    <MenuActionButton
                      color="#2F2F2F"
                      onClick={() => setShowMenuPopUp(sale.id)}
                      _hover={{
                        bgColor: "neutral.200",
                      }}
                      _active={{
                        bgColor: "neutral.300",
                      }}
                    />
                  </Td>
                  {showMenuPopUp === sale.id && (
                    <MenuPopUp
                      top="-4px"
                      right="36px"
                      options={menuOptions(sale)}
                      setShowMenuPopUp={setShowMenuPopUp}
                    />
                  )}
                </T.Body>
              ))}
            </Tbody>
            <Tfoot w="100%" h="15px"></Tfoot>
          </Table>
          <Pagination page={page} lastPage={lastPage} onClick={setPage} />
        </TableContainer>
        <StatusModal
          isOpen={statusIsOpen}
          onClose={statusOnClose}
          onSubmit={onSubmit}
          saleId={showMenuPopUp!}
        />
        {saleToEdit && (
          <EditFormModal
            isOpen={isOpenEditForm}
            onClose={() => setIsOpenEditForm(false)}
            onSubmit={onEditSubmit}
            storeId={Number(storeId)}
            sale={saleToEdit}
            setIsLoading={setIsLoading}
          />
        )}
        {selectedSale && (
          <PixModal
            key={selectedSale.id}
            isOpen={pixIsOpen}
            onClose={pixOnClose}
            price={Number(selectedSale.price)}
            pixCopyPasteValue={selectedSale.paymentCopyPaste}
            expirationDate={selectedSale.expirationDate}
            expirationHour={selectedSale.expirationHour}
          />
        )}
      </Stack>
    </MainContainer>
  );
};
