import {
  Button,
  Modal,
  ModalBody,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Stack,
  Text,
  useBreakpointValue,
} from "@chakra-ui/react";
import { moneyFormat } from "@/utils/formatUtil";
import { useCustomToast } from "@/hooks/useToast";
import { QRCodeCanvas } from "qrcode.react";

type Props = {
  isOpen: boolean;
  onClose: () => void;
  price: number;
  pixCopyPasteValue?: string;
  expirationDate?: string;
  expirationHour?: string;
};

export const PixModal = ({
  isOpen,
  onClose,
  price,
  pixCopyPasteValue,
  expirationDate,
  expirationHour,
}: Props) => {
  const { customToast } = useCustomToast();
  const isMobile = useBreakpointValue({ base: true, md: false });

  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered>
      <ModalOverlay />
      <ModalContent
        bgColor="neutral.100"
        w={{ base: "250px", md: "400px" }}
        pb={5}
      >
        <ModalHeader>
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="700"
            fontSize="24px"
            lineHeight="32px"
            color="neutral.600"
            textAlign="center"
          >
            Código PIX
          </Text>
        </ModalHeader>
        <ModalBody>
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="500"
            fontSize="14px"
            lineHeight="22px"
            color="neutral.600"
            textAlign="center"
          >
            Total do Pedido: {moneyFormat(price)}
          </Text>
          <Stack justifyContent="center" alignItems="center">
            <Stack
              bgColor="neutral.150"
              h={{ base: "180px", md: "350px" }}
              w={{ base: "180px", md: "350px" }}
              justifyContent="center"
              alignItems="center"
              borderRadius={6}
              mb={10}
            >
              {pixCopyPasteValue && (
                <QRCodeCanvas
                  value={pixCopyPasteValue}
                  size={isMobile ? 150 : 300}
                />
              )}
            </Stack>
            <Button
              w={{ base: "180px", md: "350px" }}
              variant="ghost"
              bgColor="secondary.500"
              onClick={() => {
                navigator.clipboard.writeText(pixCopyPasteValue ?? "");
                customToast("success", "QRCode copiado com sucesso");
              }}
              _hover={{
                bgColor: "secondary.300",
              }}
              _active={{
                bgColor: "secondary.500",
              }}
              mb={3}
            >
              <Text
                fontFamily="Inter"
                fontStyle="normal"
                fontWeight="600"
                fontSize="16px"
                lineHeight="20px"
                color="neutral.0"
              >
                Copiar código Pix
              </Text>
            </Button>
            <Text
              fontFamily="Inter"
              fontStyle="normal"
              fontWeight="400"
              fontSize="16px"
              lineHeight="16px"
              color="secondary.500"
              w={{ base: "180px", md: "350px" }}
            >
              Realizar o pagamento até {expirationDate} às {expirationHour}{" "}
              horário de Brasília.
            </Text>
          </Stack>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};
