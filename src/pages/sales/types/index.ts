import { PaymentStatusType } from "../enums";

export type SaleIndexQueryDto = {
  graduateName?: string;
  buyerName?: string;
  accountName?: string;
  page: number;
};

export type SaleIndexDto = {
  id: number;
  luckyNumber: string;
  graduateCode: string;
  graduateName: string;
  graduateClass: string;
  buyerName: string;
  buyerContact: string;
  buyerEmail: string;
  storeName: string;
  storeNickname: string;
  date: string;
  hour: string;
  productName: string;
  price: string;
  paymentStatus: PaymentStatusType;
  paymentType: string;
  paymentCopyPaste?: string;
  expirationDate?: string;
  expirationHour?: string;
};

export type SaleIndexPaginatedDto = {
  data: SaleIndexDto[];
  page: number;
  perPage: number;
  lastPage: number;
  total: number;
};

export type SaleIndexFilterDto = {
  graduateName?: string;
  buyerName?: string;
  accountName?: string;
};

export type SaleExportQueryDto = SaleIndexQueryDto;

export type SaleStatusUpdateDto = {
  status: string;
};

export type SaleEditDto = {
  graduate?: number;
  graduateCode?: string;
  buyerName?: string;
  buyerContact?: string;
  buyerEmail?: string;
};
