import {
  Button,
  FormControl,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  Stack,
} from "@chakra-ui/react";
import * as Fonts from "../../../components/texts";
import Joi from "joi";
import { useForm } from "react-hook-form";
import { joiResolver } from "@hookform/resolvers/joi";
import { SaleStatusUpdateDto } from "../types";
import { useEffect, useState } from "react";

type Props = {
  isOpen: boolean;
  onClose: () => void;
  onSubmit: (id: number, data: SaleStatusUpdateDto) => void;
  saleId: number;
};

const schema = Joi.object({
  status: Joi.string().required().messages({
    "string.empty": "Status deve ser informado",
  }),
});

/* eslint-disable react/no-children-prop */
export const StatusModal = ({ isOpen, onClose, onSubmit, saleId }: Props) => {
  const [sale, setSale] = useState<number | undefined>();
  const { register, handleSubmit } = useForm<SaleStatusUpdateDto>({
    resolver: joiResolver(schema),
  });

  useEffect(() => {
    if (!saleId) return;

    setSale(saleId);
  }, [saleId]);

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      isCentered
      closeOnEsc={true}
      closeOnOverlayClick={true}
    >
      <ModalOverlay />
      <ModalContent
        left={142}
        bgColor="neutral.100"
        as="form"
        onSubmit={handleSubmit((value) => onSubmit(sale!, value))}
        w={300}
      >
        <ModalHeader>Alterar status da venda</ModalHeader>
        <ModalBody>
          <Stack spacing={5}>
            <FormControl w="100%">
              <Fonts.FormLabelText>Status</Fonts.FormLabelText>
              <Select h={10} borderRadius={4} {...register("status")}>
                <option value="concluida">Concluído</option>
                <option value="devolvido">Estornado</option>
              </Select>
            </FormControl>
          </Stack>
        </ModalBody>
        <ModalFooter>
          <Button
            variant="solid"
            type="submit"
            bgColor="secondary.500"
            w="100%"
            h={9}
            borderRadius={4}
            _hover={{
              bgColor: "secondary.300",
            }}
            _active={{
              bgColor: "secondary.500",
            }}
          >
            <Fonts.ButtonText>Confirmar</Fonts.ButtonText>
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
