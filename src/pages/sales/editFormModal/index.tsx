import {
  Box,
  Button,
  HStack,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  VStack,
} from "@chakra-ui/react";
import * as Fonts from "../../../components/texts";
import { useForm } from "react-hook-form";
import { SaleEditDto, SaleIndexDto } from "../types";
import { CompareField } from "@/components/compareField";
import { useEffect, useState } from "react";
import { GraduateByCodeDto } from "@/pages/graduates/types";
import { useGraduate } from "@/hooks/useGraduate";
import { useCustomToast } from "@/hooks/useToast";

type Props = {
  isOpen: boolean;
  onClose: () => void;
  onSubmit: (id: number, data: SaleEditDto) => void;
  storeId: number;
  sale: SaleIndexDto;
  setIsLoading: (value: boolean) => void;
};

const initialValues = {
  graduate: 0,
  graduateCode: "",
  buyerName: "",
  buyerContact: "",
  buyerEmail: "",
};

/* eslint-disable react/no-children-prop */
export const EditFormModal = ({
  isOpen,
  onClose,
  onSubmit,
  storeId,
  sale,
  setIsLoading,
}: Props) => {
  const { getGraduateByCode } = useGraduate();
  const { customToast } = useCustomToast();
  const { register, handleSubmit, watch, setValue, reset } =
    useForm<SaleEditDto>({
      defaultValues: initialValues,
    });

  const [graduade, setGraduate] = useState<GraduateByCodeDto | undefined>();

  const graduateCode = watch("graduateCode");
  const buyerName = watch("buyerName");
  const buyerContact = watch("buyerContact");
  const buyerEmail = watch("buyerEmail");

  const getGraduate = (code: string) => {
    if (!code) {
      setValue("graduateCode", "");
      setValue("graduate", 0);
      setGraduate(undefined);

      return;
    }

    setIsLoading(true);

    getGraduateByCode(storeId, code)
      .then((response) => {
        setGraduate(response.data);
        setValue("graduate", response.data.id);
      })
      .catch((error) => {
        setGraduate(undefined);
        setValue("graduate", 0);
        setValue("graduateCode", "");
        customToast("error", [error.response.data.message].join(", "));
      })
      .finally(() => setIsLoading(false));
  };

  useEffect(() => {
    if (isOpen) {
      reset();
      setGraduate(undefined);
    }
  }, [isOpen]);

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      isCentered
      closeOnEsc={true}
      closeOnOverlayClick={true}
    >
      <ModalOverlay />
      <ModalContent
        left={142}
        bgColor="neutral.100"
        as="form"
        onSubmit={handleSubmit((value) => onSubmit(sale.id, value))}
        maxW={"800px"}
      >
        <ModalHeader>
          <Box>
            Edição da venda
            <Text
              fontFamily="Manrope"
              fontStyle="normal"
              fontWeight="600"
              fontSize="15px"
              lineHeight="32px"
              color="neutral.500"
            >
              {`${sale.storeNickname}-${sale.id}`}
            </Text>
          </Box>
        </ModalHeader>
        <ModalBody>
          <HStack spacing={5}>
            <Box>
              <Text
                fontFamily="Inter"
                fontStyle="normal"
                fontWeight="700"
                fontSize="15px"
                lineHeight="32px"
                color="neutral.500"
              >
                Dados do Formando
              </Text>
              <VStack>
                <CompareField
                  label="Código do Formando"
                  from={sale.graduateCode}
                  to={graduateCode}
                  field="graduateCode"
                  register={register}
                  callback={getGraduate}
                />
                <CompareField
                  label="Nome do Formando"
                  from={sale.graduateName}
                  to={graduade?.name ?? ""}
                  register={register}
                  isDisable
                />
                <CompareField
                  label="Turma do Formando"
                  from={sale.graduateClass}
                  to={graduade?.class ?? ""}
                  register={register}
                  isDisable
                />
              </VStack>
            </Box>
            <Box>
              <Text
                fontFamily="Inter"
                fontStyle="normal"
                fontWeight="700"
                fontSize="15px"
                lineHeight="32px"
                color="neutral.500"
              >
                Dados do Comprador
              </Text>
              <VStack>
                <CompareField
                  label="Nome do Comprador"
                  from={sale.buyerName}
                  to={buyerName}
                  field="buyerName"
                  register={register}
                />
                <CompareField
                  label="Telefone do Comprador"
                  from={sale.buyerContact}
                  to={buyerContact}
                  field="buyerContact"
                  register={register}
                />
                <CompareField
                  label="E-mail do Comprador"
                  from={sale.buyerEmail}
                  to={buyerEmail}
                  field="buyerEmail"
                  register={register}
                />
              </VStack>
            </Box>
          </HStack>
        </ModalBody>
        <ModalFooter>
          <HStack w="100%" spacing={5} justifyContent="center">
            <Button
              variant="outline"
              borderColor="secondary.500"
              colorScheme="secondary.500"
              w="20%"
              h={9}
              borderRadius={4}
              onClick={onClose}
              _hover={{
                bgColor: "secondary.100",
              }}
              _active={{
                bgColor: "secondary.300",
              }}
            >
              <Fonts.ButtonText color="secondary.500">
                Cancelar
              </Fonts.ButtonText>
            </Button>
            <Button
              variant="solid"
              type="submit"
              bgColor="secondary.500"
              w="20%"
              h={9}
              borderRadius={4}
              _hover={{
                bgColor: "secondary.300",
              }}
              _active={{
                bgColor: "secondary.500",
              }}
            >
              <Fonts.ButtonText>Confirmar</Fonts.ButtonText>
            </Button>
          </HStack>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
