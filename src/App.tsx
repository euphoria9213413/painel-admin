import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import { theme } from "./theme";
import { AppRoutes } from "./routes";
import { BrowserRouter } from "react-router-dom";
import { GlobalFonts } from "./fonts";

function App() {
  return (
    <ChakraProvider theme={extendTheme(theme)}>
      <BrowserRouter>
        <GlobalFonts />
        <AppRoutes />
      </BrowserRouter>
    </ChakraProvider>
  );
}

export default App;
