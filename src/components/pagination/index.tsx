import { Button, HStack, Text } from "@chakra-ui/react";

type Prop = {
  page: number;
  lastPage: number;
  onClick: (value: number) => void;
};

export const Pagination = ({ page, lastPage, onClick }: Prop) => {
  return (
    <HStack justifyContent="space-between">
      <Text
        fontFamily="Manrope"
        fontStyle="normal"
        fontWeight="700"
        fontSize="11px"
        lineHeight="30px"
        color="neutral.350"
      >
        {`Exibindo ${page} de ${lastPage}`}
      </Text>
      <HStack spacing={5} justifyContent="flex-end">
        <Button
          variant="ghost"
          isDisabled={page === 1}
          onClick={() => onClick(page - 1)}
          _hover={{
            bgColor: "neutral.300",
          }}
          _active={{
            bgColor: "neutral.350",
          }}
        >
          <Text
            fontFamily="Manrope"
            fontStyle="normal"
            fontWeight="700"
            fontSize="11px"
            lineHeight="30px"
            color="neutral.350"
          >
            {"<"}
          </Text>
        </Button>
        <Text
          fontFamily="Manrope"
          fontStyle="normal"
          fontWeight="700"
          fontSize="11px"
          lineHeight="30px"
          color="neutral.350"
        >
          {page}
        </Text>
        <Button
          variant="ghost"
          isDisabled={page === lastPage}
          onClick={() => onClick(page + 1)}
          _hover={{
            bgColor: "neutral.300",
          }}
          _active={{
            bgColor: "neutral.350",
          }}
        >
          <Text
            fontFamily="Manrope"
            fontStyle="normal"
            fontWeight="700"
            fontSize="11px"
            lineHeight="30px"
            color="neutral.350"
          >
            {">"}
          </Text>
        </Button>
      </HStack>
    </HStack>
  );
};
