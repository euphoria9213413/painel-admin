import { Input, InputProps } from "@chakra-ui/react";

type Props = {
  placeholder?: string;
  onClick?: () => void;
} & InputProps;

export const SimpleText = ({ placeholder, onClick, ...rest }: Props) => {
  return (
    <Input
      type="text"
      placeholder={placeholder}
      _placeholder={{ color: "neutral.50" }}
      w="100%"
      h={10}
      borderRadius={3}
      textIndent="15px"
      border="1px solid #EAEAEA"
      onClick={onClick}
      bgColor="neutral.0"
      {...rest}
    />
  );
};
