import { HStack, VStack } from "@chakra-ui/react";
import { SideBar } from "./sideBar";
import { TopBar } from "./topBar";
import { ReactNode } from "react";
import { useLogin } from "@/hooks/useLogin";

type Props = {
  title?: string;
  children?: ReactNode;
  height?: string;
};

export const MainContainer = ({
  title = "Painel de controle",
  children,
  height,
}: Props) => {
  const { getAccount } = useLogin();

  return (
    <HStack
      alignItems="stretch"
      bg="neutral.200"
      flexShrink={0}
      h={height || "100vh"}
    >
      <SideBar
        menus={[
          {
            id: 1,
            name: "Minhas Lojas",
            route: "/lojas",
          },
          {
            id: 2,
            name: "Configurações",
            subMenus: [
              {
                id: 1,
                name: "Meu perfil",
                route: `/usuario/editar/${getAccount()!.id}`,
              },
              { id: 2, name: "Usuários", route: "/usuarios" },
            ],
          },
        ]}
      />
      <VStack w="100%" marginStart="0 !important" h="100%">
        <TopBar title={title} />
        {children}
      </VStack>
    </HStack>
  );
};
