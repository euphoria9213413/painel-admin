import { HStack, Text } from "@chakra-ui/react";
import { MenuPopUp } from "./menuPopUp";
import { useState } from "react";
import { MenuActionButton } from "../../menuActionButton";
import { useLogin } from "@/hooks/useLogin";

type Props = {
  title: string;
};

export const TopBar = ({ title }: Props) => {
  const [showMenuPopUp, setShowMenuPopUp] = useState(false);
  const { getAccount } = useLogin();

  const accountName = getAccount()?.name;

  return (
    <HStack
      w="100%"
      h={55}
      justifyContent="space-between"
      alignItems="center"
      bgColor="neutral.500"
      borderLeft="0.13rem solid #171717"
      pl={5}
      pr={1}
      position="relative"
    >
      <Text
        fontFamily="Manrope"
        fontStyle="normal"
        fontWeight="700"
        fontSize="19px"
        lineHeight="32px"
        color="neutral.50"
      >
        {title}
      </Text>
      <HStack
        display="flex"
        h={50}
        alignItems="center"
        alignContent="center"
        justifyContent="center"
        spacing={1}
      >
        <Text
          fontFamily="Manrope"
          fontStyle="normal"
          fontWeight="700"
          fontSize="15px"
          lineHeight="30px"
          color="neutral.50"
        >
          {accountName}
        </Text>
        <MenuActionButton onClick={() => setShowMenuPopUp(!showMenuPopUp)} />
      </HStack>
      {showMenuPopUp && <MenuPopUp />}
    </HStack>
  );
};
