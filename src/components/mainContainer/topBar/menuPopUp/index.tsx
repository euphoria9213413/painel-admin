import { useLogin } from "@/hooks/useLogin";
import { Button, Text, VStack } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

export const MenuPopUp = () => {
  const { removeAuthentication, getAccount } = useLogin();
  const navigate = useNavigate();

  return (
    <VStack
      position="absolute"
      w={125}
      h="90px"
      bgColor="neutral.500"
      top="55px"
      right={0}
      borderTop="0.13rem solid #171717"
      borderBottomStartRadius={5}
      alignItems="flex-start"
      zIndex={1000}
    >
      <Button
        display="flex"
        justifyContent="flex-start"
        w="100%"
        variant="ghost"
        onClick={() => navigate(`/usuario/editar/${getAccount()!.id}`)}
        _hover={{
          bgColor: "neutral.400",
        }}
        _active={{
          bgColor: "neutral.500",
        }}
      >
        <Text
          mt={1}
          fontFamily="Manrope"
          fontStyle="normal"
          fontWeight="700"
          fontSize="15px"
          lineHeight="30px"
          color="neutral.50"
        >
          Meu Perfil
        </Text>
      </Button>
      <Button
        display="flex"
        justifyContent="flex-start"
        w="100%"
        variant="ghost"
        onClick={() => {
          removeAuthentication();
          navigate("/login");
        }}
        _hover={{
          bgColor: "neutral.400",
        }}
        _active={{
          bgColor: "neutral.500",
        }}
      >
        <Text
          fontFamily="Manrope"
          fontStyle="normal"
          fontWeight="700"
          fontSize="15px"
          lineHeight="30px"
          color="red"
        >
          Sair
        </Text>
      </Button>
    </VStack>
  );
};
