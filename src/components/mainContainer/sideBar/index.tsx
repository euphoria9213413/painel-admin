import { Box, Button, HStack, Image, Stack, Text } from "@chakra-ui/react";
import { Menu, Props } from "./types";
import { useCallback, useState } from "react";
import { ArrowIcon } from "../../icons";
import { useNavigate } from "react-router-dom";

const logoImage = "/images/company-logo.png";

export const SideBar = ({ menus }: Props) => {
  const navigate = useNavigate();
  const [menuSelected, setMenuSelected] = useState<number | undefined>();

  const handleClick = useCallback(
    (menu: Menu) => () => {
      if (menu.route) {
        navigate(menu.route);
      }

      if (menu.id === menuSelected) {
        setMenuSelected(undefined);
        return;
      }

      setMenuSelected(menu.id);
    },
    [menuSelected]
  );

  return (
    <Box w="300px" bgColor="neutral.500">
      <Stack alignItems="center" mt={5}>
        <Image w={240} src={logoImage}></Image>
      </Stack>
      <Box mt={10}>
        {menus.map((menu) => (
          <Box mt={3} key={menu.id}>
            <Button
              w="100%"
              variant="ghost"
              _hover={{
                bgColor: "neutral.400",
              }}
              _active={{
                bgColor: "neutral.500",
              }}
            >
              <HStack
                w="100%"
                pl="45px"
                pr="40px"
                justifyContent="space-between"
                cursor="pointer"
                onClick={handleClick(menu)}
              >
                <Text
                  fontFamily="Manrope"
                  fontStyle="normal"
                  fontWeight="700"
                  fontSize="19px"
                  lineHeight="30px"
                  color="neutral.200"
                >
                  {menu.name}
                </Text>
                {menu.subMenus?.length && (
                  <Box
                    transform={
                      menuSelected === menu.id
                        ? "rotate(0deg)"
                        : "rotate(-90deg)"
                    }
                  >
                    <ArrowIcon w={10} h={10} />
                  </Box>
                )}
              </HStack>
            </Button>
            {menuSelected === menu.id && menu.subMenus?.length && (
              <Box w="100%">
                {menu.subMenus.map((subMenu) => (
                  <Button
                    key={subMenu.id}
                    pl="80px"
                    display="flex"
                    justifyContent="flex-start"
                    w="100%"
                    variant="ghost"
                    onClick={() => navigate(subMenu.route)}
                    _hover={{
                      bgColor: "neutral.400",
                    }}
                    _active={{
                      bgColor: "neutral.500",
                    }}
                  >
                    <Text
                      fontFamily="Manrope"
                      fontStyle="normal"
                      fontWeight="700"
                      fontSize="16px"
                      lineHeight="30px"
                      color="neutral.200"
                    >
                      {subMenu.name}
                    </Text>
                  </Button>
                ))}
              </Box>
            )}
          </Box>
        ))}
      </Box>
    </Box>
  );
};
