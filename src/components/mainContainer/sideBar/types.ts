type SubMenu = {
  id: number;
  name: string;
  route: string;
};

export type Menu = {
  id: number;
  name: string;
  route?: string;
  subMenus?: SubMenu[];
};

export type Props = {
  menus: Menu[];
};
