import { Box, Button, HStack, Input, Text } from "@chakra-ui/react";
import { NewIcon } from "../icons";
import { Fragment, ReactNode, useRef } from "react";

type Buttons = {
  title: string;
  type?: string;
  onClick: (value?: any) => void;
};

type Filter = {
  field?: ReactNode;
};

type Props = {
  title: string;
  subTitle?: string;
  buttons?: Buttons[];
  filters?: Filter[];
};

export const FilterBar = ({ title, subTitle, buttons, filters }: Props) => {
  const hiddenFileInput = useRef<any>(null);

  return (
    <Box
      mt="0 !important"
      bgColor="neutral.300"
      h="150px"
      w="100%"
      pl={5}
      pr={5}
    >
      <HStack mt={5} alignItems="center" justifyContent="space-between">
        <Box>
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="600"
            fontSize="20px"
            lineHeight="31px"
            color="neutral.600"
          >
            {title}
          </Text>
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="300"
            fontSize="18px"
            lineHeight="23px"
            color="neutral.600"
          >
            {subTitle}
          </Text>
        </Box>
        <HStack>
          {buttons?.map((button, index) => {
            return button.type === "inputFile" ? (
              <Fragment key={index}>
                <Button
                  leftIcon={<NewIcon />}
                  bgColor="secondary.500"
                  w={180}
                  h={9}
                  borderRadius={4}
                  onClick={() => hiddenFileInput?.current?.click()}
                  variant="solid"
                  _hover={{
                    bgColor: "secondary.300",
                  }}
                  _active={{
                    bgColor: "secondary.500",
                  }}
                >
                  <Text
                    fontFamily="Inter"
                    fontStyle="normal"
                    fontWeight="600"
                    fontSize="13px"
                    lineHeight="18px"
                    color="neutral.0"
                  >
                    {button.title}
                  </Text>
                </Button>
                <Input
                  type="file"
                  ref={hiddenFileInput}
                  onChange={(event) => {
                    button.onClick && button.onClick(event?.target?.files![0]);
                  }}
                  style={{ display: "none" }}
                />
              </Fragment>
            ) : (
              <Button
                key={index}
                leftIcon={<NewIcon />}
                bgColor="secondary.500"
                w={180}
                h={9}
                borderRadius={4}
                onClick={button.onClick}
                variant="solid"
                _hover={{
                  bgColor: "secondary.300",
                }}
                _active={{
                  bgColor: "secondary.500",
                }}
              >
                <Text
                  fontFamily="Inter"
                  fontStyle="normal"
                  fontWeight="600"
                  fontSize="13px"
                  lineHeight="18px"
                  color="neutral.0"
                >
                  {button.title}
                </Text>
              </Button>
            );
          })}
        </HStack>
      </HStack>
      <HStack mt={6}>{filters?.map((filter) => filter.field)}</HStack>
    </Box>
  );
};
