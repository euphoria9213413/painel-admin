import { Text } from "@chakra-ui/react";

type Props = {
  children: string;
  color?: string;
  fontSize?: string;
};

export const TableTheadText = ({ children, color }: Props) => {
  return (
    <Text
      fontFamily="Inter"
      fontStyle="normal"
      fontWeight="900"
      fontSize="11px"
      lineHeight="23px"
      color={color ?? "neutral.500"}
    >
      {children}
    </Text>
  );
};

export const TableBodyText = ({ children, color }: Props) => {
  return (
    <Text
      fontFamily="Inter"
      fontStyle="normal"
      fontWeight="400"
      fontSize="14px"
      lineHeight="23px"
      color={color ?? "neutral.500"}
      display="inline-table"
    >
      {children}
    </Text>
  );
};

export const FormLabelText = ({ children, color }: Props) => {
  return (
    <Text
      fontFamily="Inter"
      fontStyle="normal"
      fontWeight="400"
      fontSize="14px"
      lineHeight="18px"
      color={color ?? "neutral.400"}
      mb={2}
    >
      {children}
    </Text>
  );
};

export const FormLabelContrastText = ({ children, fontSize, color }: Props) => {
  return (
    <Text
      fontFamily="Inter"
      fontStyle="normal"
      fontWeight="700"
      fontSize={fontSize ?? "14px"}
      lineHeight="32px"
      color={color ?? "neutral.500"}
      mb={2}
    >
      {children}
    </Text>
  );
};

export const FormInputInformationText = ({ children, color }: Props) => {
  return (
    <Text
      fontFamily="Inter"
      fontStyle="normal"
      fontWeight="600"
      fontSize="14px"
      lineHeight="17px"
      color={color ?? "neutral.350"}
      mt={1}
    >
      {children}
    </Text>
  );
};

export const ButtonText = ({ children, color }: Props) => {
  return (
    <Text
      fontFamily="Inter"
      fontStyle="normal"
      fontWeight="600"
      fontSize="13px"
      lineHeight="18px"
      color={color ?? "neutral.0"}
    >
      {children}
    </Text>
  );
};
