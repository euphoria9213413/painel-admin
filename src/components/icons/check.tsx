import { Center, CenterProps } from "@chakra-ui/react";

export const CheckIcon = ({ ...rest }: CenterProps) => {
  return (
    <Center color="#62CD0F" {...rest}>
      <svg
        width="16"
        height="11"
        viewBox="0 0 16 11"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M1 5.5L5.66667 10L15 1"
          stroke="currentColor"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    </Center>
  );
};
