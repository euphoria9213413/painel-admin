import { Center, CenterProps } from "@chakra-ui/react";

export const ArrowIcon = ({ ...rest }: CenterProps) => {
  return (
    <Center color="#EAEAEA" {...rest}>
      <svg
        width="18"
        height="10"
        viewBox="0 0 18 10"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <line
          x1="0.36997"
          y1="0.436369"
          x2="8.09724"
          y2="8.93637"
          stroke="currentColor"
        />
        <line
          x1="17.3536"
          y1="0.353553"
          x2="8.08083"
          y2="9.62628"
          stroke="currentColor"
        />
      </svg>
    </Center>
  );
};
