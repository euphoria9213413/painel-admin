import { Center, CenterProps } from "@chakra-ui/react";

export const SearchIcon = ({ ...rest }: CenterProps) => {
  return (
    <Center color="#3D44AA" {...rest}>
      <svg
        width="29"
        height="26"
        viewBox="0 0 29 26"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M21.3125 20.3125L17.1339 16.1339M18.5267 11.6072C18.5267 14.8765 15.8764 17.5268 12.607 17.5268C9.33771 17.5268 6.68738 14.8765 6.68738 11.6072C6.68738 8.33783 9.33771 5.6875 12.607 5.6875C15.8764 5.6875 18.5267 8.33783 18.5267 11.6072Z"
          stroke="currentColor"
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    </Center>
  );
};
