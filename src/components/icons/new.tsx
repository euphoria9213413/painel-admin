import { Center, CenterProps } from "@chakra-ui/react";

export const NewIcon = ({ ...rest }: CenterProps) => {
  return (
    <Center color="white" {...rest}>
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <circle
          cx="11.5938"
          cy="11.5938"
          r="7.84375"
          transform="rotate(-90 11.5938 11.5938)"
          stroke="currentColor"
          strokeWidth="1.5"
        />
        <path
          d="M11.5592 7.96036V15.2271"
          stroke="currentColor"
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M7.92578 11.5938H15.1926"
          stroke="currentColor"
          strokeWidth="1.5"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    </Center>
  );
};
