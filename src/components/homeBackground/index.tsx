import React from "react";
import { Flex, HStack, Image, Text, VStack } from "@chakra-ui/react";

type Props = {
  children: React.ReactNode;
};

const logoImage = "/images/company-logo.png";

export const HomeBackground = ({ children }: Props) => {
  return (
    <Flex
      h="100vh"
      bgGradient="linear(to-r, neutral.500 60%, secondary.500 40%)"
      alignItems="center"
      justifyContent="center"
    >
      <HStack spacing={300}>
        <VStack>
          <Image src={logoImage} />
          <Text
            fontFamily="Inter"
            fontStyle="normal"
            fontWeight="700"
            fontSize="25px"
            lineHeight="50px"
            letterSpacing="0.17em"
            color="neutral.20"
          >
            Sua lojinha, na sua mão
          </Text>
        </VStack>
        {children}
      </HStack>
    </Flex>
  );
};
