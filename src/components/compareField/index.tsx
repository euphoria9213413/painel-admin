import { Box, HStack, Input, VStack } from "@chakra-ui/react";
import * as Fonts from "../texts";
import { UseFormRegister } from "react-hook-form";
import { SaleEditDto } from "@/pages/sales/types";
import InputMask from "react-input-mask";

type Props = {
  isDisable?: boolean;
  label: string;
  from: string;
  to?: string;
  field?: "graduateCode" | "buyerName" | "buyerContact" | "buyerEmail";
  register: UseFormRegister<SaleEditDto>;
  callback?: (code: string) => void;
};

export const CompareField = ({
  isDisable,
  label,
  from,
  to,
  field,
  register,
  callback,
}: Props) => {
  const reg = field ? { ...register(field) } : {};

  return (
    <Box ml={2}>
      <Fonts.FormLabelContrastText>{label}</Fonts.FormLabelContrastText>
      <Box ml={3}>
        <HStack spacing={3}>
          <VStack alignItems={"start"}>
            <Fonts.FormLabelContrastText fontSize="13px">
              De:
            </Fonts.FormLabelContrastText>
            <Fonts.FormLabelContrastText fontSize="13px" color="secondary.500">
              Para:
            </Fonts.FormLabelContrastText>
          </VStack>
          <VStack>
            <Input
              as={InputMask}
              mask={field == "buyerContact" ? "(**) *****-****" : ""}
              type="text"
              textIndent={"1px"}
              w="300px"
              h={10}
              borderRadius={5}
              border="1px solid #BCBCBC"
              bgColor="neutral.0"
              isDisabled={true}
              value={from}
              fontWeight={500}
            />
            {field === "buyerContact" ? (
              <Input
                as={InputMask}
                mask={"(**) *****-****"}
                type="text"
                textIndent={"1px"}
                w="300px"
                h={10}
                borderRadius={5}
                border="1px solid #BCBCBC"
                bgColor="neutral.0"
                {...reg}
                value={to ?? ""}
                isDisabled={isDisable}
                onBlur={(event) => callback && callback(event.target.value)}
              />
            ) : (
              <Input
                type="text"
                textIndent={"1px"}
                w="300px"
                h={10}
                borderRadius={5}
                border="1px solid #BCBCBC"
                bgColor="neutral.0"
                {...reg}
                value={to ?? ""}
                isDisabled={isDisable}
                onBlur={(event) => callback && callback(event.target.value)}
              />
            )}
          </VStack>
        </HStack>
      </Box>
    </Box>
  );
};
