import { Box, Spinner } from "@chakra-ui/react";

const LoadingOverlay = () => {
  return (
    <Box
      position="fixed"
      top={0}
      left={0}
      right={0}
      bottom={0}
      display="flex"
      justifyContent="center"
      alignItems="center"
      bg="rgba(0, 0, 0, 0.5)"
      zIndex={9999}
    >
      <Spinner
        thickness="4px"
        speed="0.65s"
        emptyColor="gray.200"
        color="blue.500"
        size="xl"
        justifyContent="center"
        alignItems="center"
        ml="250px"
      />
    </Box>
  );
};

export default LoadingOverlay;
